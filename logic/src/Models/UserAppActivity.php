<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class UserAppActivity extends BaseModel
{
    protected $fillable = [
        'client',
        'app_version_id',
        'app_channel_id',
        'num',
    ];

    public function appVersion()
    {
        return $this->belongsTo(AppVersion::class);
    }

    public function appChannel()
    {
        return $this->belongsTo(AppChannel::class);
    }
}
