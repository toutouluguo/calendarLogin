<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class AppBehaviorLog extends BaseModel
{
    protected $fillable = [
        'device_id',
        'app_channel_id',
        'vivo_user_id',
        'data',
        'response_data',
        'type',
        'status',
        'push_platform'
    ];

    protected $casts = [
        'data'          => 'array',
        'response_data' => 'array',
    ];
}
