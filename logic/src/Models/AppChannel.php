<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class AppChannel extends BaseModel
{
    protected $fillable = [
        'name',
        'channel',
    ];
}
