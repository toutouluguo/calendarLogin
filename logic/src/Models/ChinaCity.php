<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

/**
 * App\Models\ChinaCity.
 *
 * @property int                             $id
 * @property int                             $location_id      位置ID
 * @property string                          $location_name_en 位置名_英文
 * @property string                          $location_name_zh 位置名_中文
 * @property string                          $country_code     国家代码_根据ISO 3166获取
 * @property string                          $country_name_en  国家名_英文
 * @property string                          $country_name_zh  国家名_中文
 * @property string                          $adm1_name_en     一级行政区划_中文
 * @property string                          $adm1_name_zh     一级行政区划_英文
 * @property string                          $adm2_name_en     次级行政区划_中文
 * @property string                          $adm2_name_zh     次级行政区划_英文
 * @property string                          $timezone         时区
 * @property string                          $latitude         纬度
 * @property string                          $longitude        经度
 * @property string                          $adcode           中国行政区域编码
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ChinaCity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChinaCity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChinaCity query()
 * @method static \Illuminate\Database\Eloquent\Builder|ChinaCity whereAdcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChinaCity whereAdm1NameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChinaCity whereAdm1NameZh($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChinaCity whereAdm2NameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChinaCity whereAdm2NameZh($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChinaCity whereCountryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChinaCity whereCountryNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChinaCity whereCountryNameZh($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChinaCity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChinaCity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChinaCity whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChinaCity whereLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChinaCity whereLocationNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChinaCity whereLocationNameZh($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChinaCity whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChinaCity whereTimezone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChinaCity whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ChinaCity extends BaseModel
{
    protected $fillable = [
        'location_id',
        'adcode',
        'adm1_name_en',
        'adm1_name_zh',
        'adm2_name_en',
        'adm2_name_zh',
        'country_code',
        'country_name_en',
        'country_name_zh',
        'location_name_en',
        'location_name_zh',
        'latitude',
        'longitude',
        'timezone',
    ];
}
