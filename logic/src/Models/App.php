<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class App extends BaseModel
{
    protected $casts = [
        'app_channel_ids' => 'array',
    ];

    public function appVersions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(AppVersion::class);
    }
}
