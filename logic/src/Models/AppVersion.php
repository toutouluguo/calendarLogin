<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class AppVersion extends BaseModel
{
    protected $attributes = [
        'version_code' => '',
        'download_url' => '',
    ];
    protected $casts = [
        'audit' => 'array',
    ];

    public function app(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(App::class);
    }
}
