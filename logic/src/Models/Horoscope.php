<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

/**
 * App\Models\Horoscope.
 *
 * @property int                             $id
 * @property string                          $constellation 星座
 * @property string                          $tab           运势标签：today,tomorrow,week,month,year,love
 * @property array|null                      $fortune_score 运势分值
 * @property array|null                      $fortune_desc  运势描述
 * @property array|null                      $evaluate_data 其他评估数据
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Horoscope newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Horoscope newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Horoscope query()
 * @method static \Illuminate\Database\Eloquent\Builder|Horoscope whereConstellation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Horoscope whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Horoscope whereEvaluateData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Horoscope whereFortuneDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Horoscope whereFortuneScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Horoscope whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Horoscope whereTab($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Horoscope whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Horoscope extends BaseModel
{
    protected $fillable = [
        'tab',
        'constellation',
        'evaluate_data',
        'fortune_desc',
        'fortune_score',
    ];

    protected $casts = [
        'fortune_score' => 'array',
        'fortune_desc'  => 'array',
        'evaluate_data' => 'array',
    ];

    public static array $constellations = [
        'aries',
        'taurus',
        'gemini',
        'cancer',
        'leo',
        'virgo',
        'libra',
        'scorpio',
        'sagittarius',
        'capricorn',
        'aquarius',
        'pisces',
    ];
}
