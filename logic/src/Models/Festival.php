<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class Festival extends BaseModel
{
    protected $fillable = [
        'festival_category_id',
        'type',
        'name',
        'date_operation',
        'content',
    ];

    protected $casts = [
        'date_operation' => 'array',
    ];

    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(FestivalCategory::class, 'festival_category_id', 'id');
    }
}
