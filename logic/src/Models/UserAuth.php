<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class UserAuth extends BaseModel
{
    protected $fillable = [
        'user_id',
        'identity_type',
        'identifier',
        'access_data',
        'user_data',
    ];
    protected $casts = [
        'access_data' => 'array',
        'user_data'   => 'array',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
