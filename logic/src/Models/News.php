<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class News extends BaseModel
{
    use SoftDeletes;
    protected $fillable = [
        'from',
        'from_id',
        'title',
        'release_at',
        'category_id',
        'author_name',
        'url',
        'pics',
        'content',
        'status',
        'introduction',
        'fortune',
    ];
    protected $casts = [
        'pics'       => 'array',
        'release_at' => 'datetime',
    ];

    protected $attributes = [
        'fortune' => '',
    ];

    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(NewsCategory::class);
    }

    public function getPicsFullUrlAttribute()
    {
        $pics = [];
        if ($this->pics) {
            foreach ($this->pics as $pic) {
                $pics[] = res_url($pic);
            }
        }

        return $pics;
    }
}
