<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class ToolsWishingLightsPray extends BaseModel
{
    public $incrementing = false;
    protected $fillable  = [
        'tools_wishing_lights_id',
        'user_id',
    ];
}
