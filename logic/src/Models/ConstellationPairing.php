<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

/**
 * App\Models\ConstellationPairing.
 *
 * @property int                             $id
 * @property string                          $male         男星座
 * @property string                          $female       女星座
 * @property int                             $idx          配对指数
 * @property string                          $proportion   配对比重
 * @property int                             $affectionate 两情相悦指数
 * @property int                             $everlasting  天长地久指数
 * @property string                          $result       结果评述
 * @property string                          $advice       恋爱建议
 * @property string                          $precaution   注意事项
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ConstellationPairing newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ConstellationPairing newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ConstellationPairing query()
 * @method static \Illuminate\Database\Eloquent\Builder|ConstellationPairing whereAdvice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConstellationPairing whereAffectionate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConstellationPairing whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConstellationPairing whereEverlasting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConstellationPairing whereFemale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConstellationPairing whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConstellationPairing whereIdx($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConstellationPairing whereMale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConstellationPairing wherePrecaution($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConstellationPairing whereProportion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConstellationPairing whereResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConstellationPairing whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ConstellationPairing extends BaseModel
{
    protected $fillable = [
        'advice',
        'affectionate',
        'everlasting',
        'female',
        'idx',
        'male',
        'precaution',
        'proportion',
        'result',
    ];
}
