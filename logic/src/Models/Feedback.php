<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;


class Feedback extends BaseModel
{
    public static array $positionArr = [
        1  => '意见反馈',
        2  => '工具-星座运程',
        3  => '工具-指纹预测',
        4  => '工具-手相预测',
        5  => '工具-生肖运程',
        6  => '工具-痣相预测',
        7  => '工具-星座匹配',
        8  => '工具-周公解梦',
        9  => '工具-八字精批',
        10 => '工具-起名测名',
        11 => '工具-号码吉凶',
        12 => '工具-八字合婚',
        13 => '工具-答案之书',
        14 => '工具-经期预测',
        15 => '黄历白话文解析页面',
        16 => '当前浏览页面',
    ];
    protected $fillable = ['content', 'images', 'contact', 'type', 'position', 'app_device_id', 'ip'];

    protected $casts = [
        'images'  => 'array',
        'contact' => 'string',
    ];
    protected $attributes = [
        'contact' => '',
    ];

    public function device(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(AppDevice::class, 'app_device_id');
    }
}
