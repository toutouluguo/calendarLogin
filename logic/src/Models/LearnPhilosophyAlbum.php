<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class LearnPhilosophyAlbum extends BaseModel
{
    public function learnPhilosophies(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(LearnPhilosophy::class);
    }

    public function learnPhilosophyCategory(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(LearnPhilosophyCategory::class);
    }
}
