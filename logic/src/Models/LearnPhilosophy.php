<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class LearnPhilosophy extends BaseModel
{
    public function learnPhilosophyAlbum(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(LearnPhilosophyAlbum::class);
    }
}
