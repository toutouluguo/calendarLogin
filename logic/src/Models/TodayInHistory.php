<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class TodayInHistory extends BaseModel
{
    protected $fillable = [
        'date',
        'history',
    ];
    protected $casts = [
        'history' => 'array',
    ];
}
