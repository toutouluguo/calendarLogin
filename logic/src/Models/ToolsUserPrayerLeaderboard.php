<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;

class ToolsUserPrayerLeaderboard extends BaseModel
{
    protected $fillable = [
        'type',
        'number',
        'content',
        'started_at',
        'ended_at',
    ];
    protected $casts = [
        'started_at' => 'datetime:Y-m-d H:i:s',
        'ended_at'   => 'datetime:Y-m-d H:i:s',
        'content'    => 'array',

    ];

    protected $attributes = [
        'number'=> 1,
    ];
    protected $dates = [
        'started_at',
        'ended_at',
    ];
    protected static array $typeText = [
        1 => '最火许愿灯',
        2 => '最亮许愿灯',
    ];

    public function toolsUserPrayerWinningRecords(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ToolsUserPrayerWinningRecord::class);
    }

    /**
     * Determine if the user is an administrator.
     */
    protected function typetext(): Attribute
    {
        return Attribute::make(
            get: fn () => self::$typeText[$this->type],
        );
    }
}
