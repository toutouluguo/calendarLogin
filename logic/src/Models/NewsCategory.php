<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class NewsCategory extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'label',
        'name',
        'sort',
        'show_type',
        'api_url',
    ];

    public static $showType = [
        1 => '普通', 2 => '美文', 3 => '运程', 4 => '每日一言', 5 => '笑话',
    ];
}
