<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class MenstruationSetting extends BaseModel
{
    protected $fillable = [
        'user_id',
        'days',
        'cycle',
        'is_home_display',
        'is_remind',
        'remind_day',
    ];
}
