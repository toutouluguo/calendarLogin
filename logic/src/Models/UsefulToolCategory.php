<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsefulToolCategory extends Model
{
    use HasDateTimeFormatter;
    use SoftDeletes;

    protected $fillable = [
        'status',
        'name',
        'sort',
        'banner',
        'show_count',
        'label',
    ];
    protected $casts = [
        'banner' => 'array',
    ];
    protected $attributes = [
        'banner' => [],
    ];
    protected $table = 'useful_tool_categories';

    public function usefulTools()
    {
        return $this->hasMany(UsefulTool::class, 'useful_tool_category_id', 'id')->orderByDesc('sort');
    }

    public function getBannerInfoAttribute()
    {
        $banners = [];
        if ($this->banner) {
            $usefulTools = UsefulTool::query()->whereIn('id', collect($this->banner)->pluck('tool_id'))->get();
            foreach ($this->banner as $banner) {
                $usefulTool = $usefulTools->where('id', $banner['tool_id'])->first();
                $banners[]  = [
                    'img'  => res_url($banner['img']),
                    'url'  => $usefulTool->url,
                    'name' => $usefulTool->name,
                ];
            }
        }

        return $banners;
    }
}
