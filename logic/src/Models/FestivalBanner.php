<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class FestivalBanner extends BaseModel
{
    /**
     * 节假日.
     */
    public function festival(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Festival::class, 'content', 'id')->where('type', 1);
    }
}
