<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

/**
 * App\Models\DailySignature.
 *
 * @property int                             $id
 * @property int                             $idx        第几签
 * @property string                          $name       签名
 * @property string                          $sign       签文
 * @property string                          $level      签级
 * @property string                          $poetry     诗意
 * @property string                          $analysis   解曰
 * @property string                          $essence    本签精髓
 * @property array                           $content    详细白话文解析
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder|DailySignature newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DailySignature newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DailySignature query()
 * @method static \Illuminate\Database\Eloquent\Builder|DailySignature whereAnalysis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DailySignature whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DailySignature whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DailySignature whereEssence($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DailySignature whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DailySignature whereIdx($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DailySignature whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DailySignature whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DailySignature wherePoetry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DailySignature whereSign($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DailySignature whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DailySignature extends BaseModel
{
    protected $fillable = [
        'idx',
        'name',
        'sign',
        'level',
        'poetry',
        'analysis',
        'essence',
        'content',
    ];

    protected $casts = [
        'content' => 'array',
    ];
}
