<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class DailyBriefing extends BaseModel
{
    protected $fillable = ['date', 'news', 'head_img'];

    protected $casts = [
        'date' => 'datetime:Y-m-d',
        'news' => 'array',
    ];
}
