<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

class ToolsUserPrayer extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'tools_prayer_lights_id',
        'name',
        'birthday',
        'wish',
        'pray',
        'show_square',
        'continuous_day',
        'auto_light_date',
        'last_light_at',
        'total_day',
    ];

    protected $casts = [
        'auto_light_date' => 'date:Y-m-d',
        'last_light_at'   => 'datetime',
    ];
    protected $dates = ['deleted_at'];

    /**
     * 点灯记录.
     */
    public function toolsPrayLightRecords(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ToolsPrayLightRecord::class, 'tools_user_prayer_id', 'id');
    }

    public function scopeShowSquare($query)
    {
        return $query->where('show_square', 1);
    }

    /**
     * 今天是否点亮.
     */
    public function lightToday(): bool
    {
        if ($this->last_light_at && Carbon::today()->toDateString() == $this->last_light_at->toDateString()) {
            return true;
        }
        // 系统已经点亮
        if ($this->auto_light_date && Carbon::today()->lte($this->auto_light_date)) {
            return true;
        }

        return false;
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function toolsPrayerLights(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ToolsPrayerLights::class);
    }

    public function toolsUserPrayerBlesses(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ToolsUserPrayerBless::class);
    }
}
