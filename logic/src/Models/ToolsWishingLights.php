<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class ToolsWishingLights extends BaseModel
{
    protected $fillable = [
        'user_id',
        'name',
        'wish',
        'img',
        'light',
        'show_square',
    ];
}
