<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class UserDailySignature extends BaseModel
{
    protected $fillable = [
        'user_id',
        'daily_signature_id',
    ];
}
