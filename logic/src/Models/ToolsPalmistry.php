<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class ToolsPalmistry extends BaseModel
{
    protected $casts = [
        'love'     => 'array',
        'marriage' => 'array',
        'wisdom'   => 'array',
        'career'   => 'array',
        'life'     => 'array',
    ];
}
