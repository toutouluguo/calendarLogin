<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class ToolsFingerprint extends BaseModel
{
    protected $fillable = [
        'mz',
        'sz',
        'zz',
        'wmz',
        'xz',
        'analyze',
        'fingerprint',
        'poetry',
        'character',
        'marriage',
        'profession',
        'healthy',
        'fortune',
    ];
}
