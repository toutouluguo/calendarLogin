<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class LearnPhilosophyView extends BaseModel
{
    protected $fillable = [
        'learn_philosophy_id',
        'imei',
    ];
}
