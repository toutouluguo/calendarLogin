<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class LearnPhilosophyCategory extends BaseModel
{
    protected $fillable = [
        'name',
        'bottom_tools',
    ];
    protected $casts = [
        'bottom_tools' => 'array',
    ];

    public function LearnPhilosophyAlbums(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(LearnPhilosophyAlbum::class);
    }
}
