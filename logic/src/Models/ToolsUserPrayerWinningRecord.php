<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class ToolsUserPrayerWinningRecord extends BaseModel
{
    protected $fillable = [
        'user_id',
        'tools_user_prayer_id',
        'tools_user_prayer_leaderboard_id',
        'bonus',
        'ranking',
        'type',
        'notice_read',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function toolsUserPrayer()
    {
        return $this->belongsTo(ToolsUserPrayer::class);
    }

    public function toolsUserPrayerLeaderboard()
    {
        return $this->belongsTo(ToolsUserPrayerLeaderboard::class);
    }
}
