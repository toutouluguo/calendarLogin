<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

use Illuminate\Support\Carbon;

class ToolsUserPrayerBless extends BaseModel
{
    protected $fillable = [
        'tools_user_prayer_id',
        'user_id',
        'imei',
        'ip',
        'is_share',
    ];

    public function scopeBlessByTimeType($query, $timeType, Carbon|string|null $carbon)
    {
        if (!$carbon) {
            $carbon = Carbon::today();
        }

        if (is_string($carbon)) {
            $carbon = Carbon::parse($carbon);
        }

        switch ($timeType) {
            case 'week':
                $start = $carbon->startOfWeek()->startOfDay()->toDateTimeString();
                $end   = $carbon->endOfWeek()->endOfDay()->toDateTimeString();
                break;
            case 'month':
                $start = $carbon->startOfMonth()->startOfDay()->toDateTimeString()();
                $end   = $carbon->endOfMonth()->endOfDay()->toDateTimeString();
                break;
            case 'year':
                $start = $carbon->startOfYear()->startOfDay()->toDateTimeString();
                $end   = $carbon->endOfYear()->endOfDay()->toDateTimeString();
                break;
        }

        return $query->whereBetween('created_at', [$start, $end]);
    }
}
