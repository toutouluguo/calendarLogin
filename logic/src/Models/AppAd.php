<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class AppAd extends BaseModel
{
    protected $fillable = [
        'label',
        'position',
        'explain',
    ];
}
