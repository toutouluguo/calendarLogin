<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

use Illuminate\Support\Carbon;

class ToolsPrayLightRecord extends BaseModel
{
    protected $fillable = [
        'user_id',
        'tools_user_prayer_id',
        'type',
        'record_date',
    ];

    protected $casts = [
        'record_date' => 'date',
    ];
    protected $dates = [
        'record_date',
    ];

    public function toolsUserPrayer()
    {
        return $this->belongsTo(ToolsUserPrayer::class);
    }

    public function scopeLightUpByTimeType($query, string $timeType, Carbon|string|null $carbon)
    {
        if (!$carbon) {
            $carbon = Carbon::today();
        }

        if (is_string($carbon)) {
            $carbon = Carbon::parse($carbon);
        }

        switch ($timeType) {
            default:
            case 'week':
                $start = $carbon->startOfWeek()->toDateString();
                $end   = $start->endOfWeek()->toDateString();
                break;
            case 'month':
                $start = $carbon->startOfMonth()->toDateString();
                $end   = $carbon->endOfMonth()->toDateString();
                break;
            case 'year':
                $start = $carbon->startOfYear()->toDateString();
                $end   = $start->endOfYear()->toDateString();
                break;
        }

        return $query->whereBetween('record_date', [$start, $end]);
    }
}
