<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;
use Illuminate\Database\Eloquent\SoftDeletes;

class DailyWord extends BaseModel
{
    use HasDateTimeFormatter;
    use SoftDeletes;

    protected $table = 'daily_words';

    protected $fillable = [
        'category_id',
        'word',
        'thumb',
        'img',
        'show_date',
    ];

    protected $casts = [
        'show_date' => 'date:Y-m-d',
    ];
    protected $attributes = [
        'category_id' => 6,
    ];

    public function newsCategory()
    {
        return $this->belongsTo(NewsCategory::class, 'category_id', 'id');
    }
}
