<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class AppDevice extends BaseModel
{
    protected $fillable = [
        'imei',
        'umeng_device_token',
        'app_channel_id',
        'headers',
        'app_id',
        'location_id',
        'coordinate',
        'ip'
    ];

    protected $casts = [
        'headers' => 'array',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function appChannel()
    {
        return $this->belongsTo(AppChannel::class);
    }

    public function app()
    {
        return $this->belongsTo(App::class, 'app_id');
    }
}
