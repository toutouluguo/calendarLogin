<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

/**
 * App\Models\Dream.
 *
 * @property int                             $id
 * @property int                             $parent_id  父ID
 * @property string                          $name       名称
 * @property string                          $content    描述
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Dream newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Dream newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Dream query()
 * @method static \Illuminate\Database\Eloquent\Builder|Dream whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dream whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dream whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dream whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dream whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dream whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Dream extends BaseModel
{
    protected $fillable = [
        'id',
        'name',
        'parent_id',
        'content',
    ];

    protected $casts = [
        'fortune_score' => 'array',
        'fortune_desc'  => 'array',
        'evaluate_data' => 'array',
    ];

    /**
     * 非分类.
     *
     * @return mixed
     */
    public function scopeDream($query)
    {
        return $query->where('parent_id', '<>', 0);
    }
}
