<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class ToolsPrayerLights extends BaseModel
{
    protected $fillable = [
        'name',
        'description',
        'effect',
        'img',
        'sort',
        'crowd',
    ];

    protected $casts = [
        'effect' => 'array',
    ];

    public function toolsUserPrayeies(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ToolsUserPrayer::class, 'tools_prayer_lights_id', 'id');
    }
}
