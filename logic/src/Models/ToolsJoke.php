<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ToolsJoke extends Model
{
    use HasDateTimeFormatter;
    use SoftDeletes;

    protected $table = 'tools_jokes';

    protected $fillable = [
        'category_id',
        'joke',
    ];

    protected $attributes = [
        'category_id' => 5,
    ];
}
