<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class AppSetting extends BaseModel
{
    protected $fillable = [
        'app_id',
        'home_tools',
        'home_url',
        'audit_app_version_id',
        'app_ads',
        'charge_ads',
    ];
    protected $attributes = [
        'app_id',
        'home_tools',
        'home_url',
        'audit_app_version_id',
        'app_ads',
    ];

    protected $casts = [
        'audit_app_version_id' => 'array',
        'app_id'               => 'array',
        'home_url'             => 'array',
        'home_tools'           => 'array',
        'app_ads'              => 'array',
        'charge_ads'           => 'array',
    ];

    public function getHomeToolssAttribute()
    {
        return $this->home_tools;
    }

    public function setHomeToolssAttribute($value)
    {
        $this->home_tools = $value;
    }
}
