<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class UsefulTool extends BaseModel
{
    protected $fillable = [
        'useful_tool_category_id',
        'name',
        'url',
        'icon',
        'status',
        'type',
        'sort',
        'is_share',
        'activity_id',
        'url_id',
    ];

    public function usefulToolCategory()
    {
        return $this->belongsTo(UsefulToolCategory::class, 'useful_tool_category_id');
    }

    public function pageActivity()
    {
        return $this->belongsTo(AppPageActivity::class, 'activity_id');
    }

    public function pageUrl()
    {
        return $this->belongsTo(AppPageUrl::class, 'url_id');
    }
}
