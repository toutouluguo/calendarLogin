<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

/**
 * App\Models\Zodiac.
 *
 * @property int                             $id
 * @property string                          $type          生肖
 * @property array                           $month_fortune 每月运势
 * @property array                           $summary       运程综述
 * @property array                           $career        事业
 * @property array                           $love          爱情
 * @property array                           $healthy       健康
 * @property array                           $wealth        财运
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Zodiac newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Zodiac newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Zodiac query()
 * @method static \Illuminate\Database\Eloquent\Builder|Zodiac whereCareer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Zodiac whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Zodiac whereHealthy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Zodiac whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Zodiac whereLove($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Zodiac whereMonthFortune($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Zodiac whereSummary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Zodiac whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Zodiac whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Zodiac whereWealth($value)
 * @mixin \Eloquent
 */
class Zodiac extends BaseModel
{
    protected $fillable = [
        'type',
        'month_fortune',
        'career',
        'healthy',
        'love',
        'summary',
        'wealth',
    ];

    protected $casts = [
        'month_fortune' => 'array',
        'career'        => 'array',
        'healthy'       => 'array',
        'love'          => 'array',
        'summary'       => 'array',
        'wealth'        => 'array',
    ];

    public static array $types = [
        'shu',
        'niu',
        'hu',
        'tu',
        'long',
        'she',
        'ma',
        'yang',
        'hou',
        'ji',
        'gou',
        'zhu',
    ];
}
