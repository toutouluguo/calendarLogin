<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class FestivalCategory extends BaseModel
{
    protected $fillable = [
        'name',
    ];
}
