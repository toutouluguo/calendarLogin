<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Models;

class AppNotification extends BaseModel
{
    protected $casts = [
        'start_time'  => 'datetime',
        'expire_time' => 'datetime',
    ];

    public static array $activity = [
        1 => '黄历页面',
        2 => '资讯页面',
        3 => '天气页面',
    ];
}
