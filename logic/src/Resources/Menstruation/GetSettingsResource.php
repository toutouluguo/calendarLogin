<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Menstruation;

use CalendarLogic\Models\Menstruation;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class GetSettingsResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $data = Menstruation::query()->where('user_id', $this->resource->user_id)->value('data');

        return [
            'days'          => (int) $this->resource->days,
            'cycle'         => (int) $this->resource->cycle,
            'isHomeDisplay' => (int) $this->resource->is_home_display,
            'isRemind'      => (int) $this->resource->is_remind,
            'remindDay'     => (int) $this->resource->remind_day,
            'hasData'       => isset($data['startDay']),
        ];
    }
}
