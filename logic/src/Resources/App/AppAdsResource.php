<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\App;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AppAdsResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'open'   => (int) $this->resource['open'],
            'adId'   => (string) $this->resource[strtolower($request->header('client')) . '_id'],
            'adType' => (string) $this->resource['ad_type'],
        ];
    }
}
