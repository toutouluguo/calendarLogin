<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\App;

use CalendarLogic\Models\UserAuth;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AccountInfoResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return \Illuminate\Support\Collection
     */
    public function toArray($request)
    {
        return UserAuth::query()->where('user_id', $request->user()->id)->pluck('identity_type');
    }
}
