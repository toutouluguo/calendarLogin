<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\App;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SettingResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $channelName = $request->header('channelName', 'official');
        $audit       = (int) isset($this->resource->audit[$channelName]) ? $this->resource->audit[$channelName]['audit'] : 0;

        return [
            'audit' => $audit,
        ];
    }
}
