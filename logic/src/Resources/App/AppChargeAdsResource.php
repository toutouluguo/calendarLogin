<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\App;

use CalendarLogic\Repositories\AppRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AppChargeAdsResource extends JsonResource
{
    protected AppRepository $appRepository;

    public function __construct($resource)
    {
        parent::__construct($resource);
        $this->appRepository = new AppRepository();
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        // app审核模式
        $audit = $this->appRepository->getAppAudit($request);

        $data = [];
        $open = 1 == $audit || 0 == $this->resource['open'] ? 0 : 1;
        foreach ($this->resource['ads'] as $ad) {
            foreach ($ad as $item) {
                $item['type'] = (int) $item['type'];
                $item['open'] = 1 == $open ? $item['open'] : 0;
                if (1 == $item['type']) {
                    $item['img'] = res_url($item['img']);
                }
                $data[$item['label']] = $item;
            }
        }

        return $data;
    }
}
