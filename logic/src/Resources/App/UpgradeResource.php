<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\App;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UpgradeResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $status = 0;
        if (!$this->resource->version_code != $request->header('versionCode')
            || !$this->resource->version_name != $request->header('version')
        ) {
            $status = 1;
        }
        if (1 == $status) {
            $status = 1 == $this->resource->update_status ? 1 : 2;
        }

        $download_url = isset($this->resource->audit[$request->header('channelName')]['apk_path'])
            ? res_url($this->resource->audit[$request->header('channelName')]['apk_path'])
            : $this->resource->download_url;
        return [
            'updateStatus'  => (int)$status,
            'versionCode'   => (int)$this->resource->version_code,
            'versionName'   => (string)$this->resource->version_name,
            'updateContent' => (string)$this->resource->update_content,
            'downloadUrl'   => (string)$download_url,
            'apkMd5'        => (string)$this->resource->apk_md5,
        ];
    }
}
