<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\UsefulTool;

use Illuminate\Http\Resources\Json\JsonResource;

class ShowToolResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'isShare'      => $this->resource->is_share,
            'name'         => $this->resource->name,
            'icon'         => res_url($this->resource->icon),
            'type'         => $this->resource->type,
            'url'          => $this->resource->pageUrl ? $this->resource->pageUrl->url : $this->resource->url,
            'pageActivity' => (string) $this->resource->pageActivity ? $this->resource->pageActivity->label : '',
        ];
    }
}
