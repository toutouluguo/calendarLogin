<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\UsefulTool;

use Illuminate\Http\Resources\Json\JsonResource;

class ShowAllCategoryAndToolsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name'  => $this->resource->name,
            'tools' => ShowToolResource::collection($this->resource->usefulTools)->toArray($request),
        ];
    }
}
