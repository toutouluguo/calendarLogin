<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\UsefulTool;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ShowCategoryResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $data = [];

        foreach ($this->resource as $item) {
            $data[$item->label] = [
                'banner'  => $item->banner_info,
                'tools'   => ShowToolResource::collection($item->usefulTools)->toArray($request),
            ];
        }

        return $data;
    }
}
