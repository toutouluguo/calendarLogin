<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\News;

use Illuminate\Http\Resources\Json\JsonResource;

class FortuneResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        if (!$pics = $this->resource->pics_full_url) {
            $pics = [res_url('news/default.png')];
        }

        return [
            'id'           => $this->resource->id,
            'title'        => (string) $this->resource->title,
            'introduction' => (string) $this->resource->introduction,
            'pics'         => $pics,
            'thumb'        => $this->thumb(),
            'fortune'      => (string) $this->resource->fortune,
            'url'          => sprintf('%snews/' . $this->resource->category->label . '/%d', config('app.tool_h5_url'), $this->resource->id),
        ];
    }

    protected function thumb()
    {
        if ($this->resource->pics) {
            $pics_arr = explode('.', $this->resource->pics[0]);

            return res_url($pics_arr[0] . '_thumb.' . $pics_arr[1]);
        }

        return '';
    }
}
