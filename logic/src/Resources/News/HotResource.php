<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\News;

use Illuminate\Http\Resources\Json\JsonResource;

class HotResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->resource->id,
            'title'        => $this->resource->title,
            'introduction' => $this->resource->introduction,
            'url'          => (string) sprintf('%snews/hot/%d', config('app.tool_h5_url'), $this->resource->id),
        ];
    }
}
