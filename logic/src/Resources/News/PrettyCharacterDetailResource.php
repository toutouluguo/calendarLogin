<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\News;

use Illuminate\Http\Resources\Json\JsonResource;

class PrettyCharacterDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->resource->id,
            'title'      => $this->resource->title,
            'author'     => $this->resource->author_name,
            'content'    => $this->resource->content,
            'authorName' => $this->resource->author_name,
        ];
    }
}
