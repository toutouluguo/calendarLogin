<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\News;

use Illuminate\Http\Resources\Json\JsonResource;

class DailyWordResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'word'  => $this->resource->word,
            'thumb' => res_url($this->resource->thumb),
            'img'   => res_url($this->resource->img),
            'date'  => $this->resource->show_date->timestamp,
            'day'   => $this->resource->show_date->format('d'),
            'month' => $this->resource->show_date->format('M'),
            'week'  => $this->resource->show_date->format('l'),
        ];
    }
}
