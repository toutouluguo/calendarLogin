<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\News;

use Illuminate\Http\Resources\Json\JsonResource;

class NewsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = [];

        $data['id']         = (int) $this->resource->id;
        $data['title']      = (string) $this->resource->title;
        $data['pics']       = (array) $this->resource->pics_full_url;
        $data['content']    = (string) $this->resource->content;
        $data['authorName'] = (string) $this->resource->author_name;
        $data['category']   = (string) $this->resource->category->name;
        $data['date']       = (string) $this->resource->release_at->format('Y-m-d');
        $data['url']        = (string) sprintf('%snews/' . $this->resource->category->label . '/%d', config('app.tool_h5_url'), $data['id']);

        return $data;
    }
}
