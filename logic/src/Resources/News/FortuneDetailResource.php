<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\News;

use Illuminate\Http\Resources\Json\JsonResource;

class FortuneDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->resource->id,
            'authorName'   => $this->resource->author_name,
            'title'        => (string) $this->resource->title,
            'introduction' => (string) $this->resource->introduction,
            'fortune'      => (string) $this->resource->fortune,
            'content'      => (string) $this->resource->content,
        ];
    }
}
