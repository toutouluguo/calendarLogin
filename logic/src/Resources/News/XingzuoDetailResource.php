<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\News;

use Illuminate\Http\Resources\Json\JsonResource;

class XingzuoDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = [];

        $data['id']         = $this->resource->id;
        $data['title']      = $this->resource->title;
        $data['pics']       = $this->resource->pics_full_url;
        $data['content']    = $this->resource->content;
        $data['authorName'] = $this->resource->from;
        $data['category']   = $this->resource->category->name;
        $data['date']       = $this->resource->release_at->toDateString();
        $data['url']        = sprintf('%snews/' . $this->resource->category->label . '/%d', config('app.tool_h5_url'), $data['id']);

        return $data;
    }
}
