<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\News;

use Illuminate\Http\Resources\Json\JsonResource;

class PrettyCharacterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        if (!$pics = $this->resource->pics_full_url) {
            $pics = [res_url('news/default.png')];
        }

        return [
            'id'           => $this->resource->id,
            'title'        => $this->resource->title,
            'introduction' => $this->resource->introduction,
            'pics'         => $pics,
            'url'          => sprintf('%snews/' . $this->resource->category->label . '/%d', config('app.tool_h5_url'), $this->resource->id),
        ];
    }
}
