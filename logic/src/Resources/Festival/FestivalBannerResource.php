<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Festival;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FestivalBannerResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $content = [];
        if (2 == $this->resource->type) {
            $content = ['url' => $this->resource->content];
        }
        if (1 == $this->resource->type) {
            $content['id']   = (int) $this->resource->content;
            $content['type'] = 1;
            if ($this->resource->festival && 2 == $this->resource->festival->festival_category_id) {
                $content['type'] = 2;
            }
        }

        return [
            'title'     => $this->resource->title,
            'img'       => res_url($this->resource->img),
            'show_date' => $this->resource->show_date,
            'type'      => $this->resource->type,
            'content'   => $content,
        ];
    }
}
