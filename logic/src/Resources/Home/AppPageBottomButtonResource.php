<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Home;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AppPageBottomButtonResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => (string) ($this->resource['title'] ?? ''),
            'img'   => res_url($this->resource['img']     ?? ''),
            'type'  => (int) $this->resource['type'],
            'url'   => (string) $this->resource['url'],

            'activity' => (int) $this->resource['activity'],
        ];
    }
}
