<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Home;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Cache;

class DailyBriefingResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        if (!$this->resource) {
            return [];
        }

//        $popup = Cache::tags('cache')->remember('dailyBriefing:' . $this->resource->date->format('Ymd') . ':' . $request->header('imei'), now()->addDay(), function () {
//            return 1;
//        });
        $popup = '';
        $news  = [];
        if (!$this->resource->news) {
            $popup = '';
        } else {
            foreach (collect($this->resource->news)->take(15)->all() as $item) {
                $news[] = ['title' => $item['title'], 'digest' => $item['digest']];
            }
        }
        $rand    = mt_rand(1, 13);
        $headImg = $this->resource->head_img ?: 'images/dailyBriefing/default' . $rand . '.jpg';

        return [
            'popup'   => (int) is_int($popup) ? 1 : 0,
            'date'    => $this->resource->date->format('Y-m-d'),
            'year'    => $this->resource->date->format('Y'),
            'month'   => $this->resource->date->format('M'),
            'day'     => $this->resource->date->format('d'),
            'headImg' => res_url($headImg),
            'news'    => $news,
        ];
    }
}
