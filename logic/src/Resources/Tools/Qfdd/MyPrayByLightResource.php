<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Tools\Qfdd;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class MyPrayByLightResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $lightToday = 0;
        if ($this->resource->lightToday()) {
            $lightToday = 1;
        }

        return [
            'id'            => (int) $this->resource->id,
            'lightsId'      => $this->resource->tools_prayer_lights_id,
            'name'          => (string) $this->resource->name,
            'birthday'      => (string) $this->resource->birthday,
            'wish'          => (string) $this->resource->wish,
            'pray'          => (int) $this->resource->pray,
            'showSquare'    => (int) $this->resource->show_square,
            'continuousDay' => (int) $this->resource->continuous_day,
            'totalDay'      => (int) $this->resource->total_day,
            'lightToday'    => (int) $lightToday,
            'hasLight'      => $this->resource->last_light_at ? 1 : 0,
            'lastLightDay'  => $this->resource->last_light_at ? Carbon::today()->diffInDays($this->resource->last_light_at->toDateString()) : 0,
        ];
    }
}
