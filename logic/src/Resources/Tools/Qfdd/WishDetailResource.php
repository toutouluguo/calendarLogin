<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Tools\Qfdd;

use CalendarLogic\Repositories\ToolsUserPrayerRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class WishDetailResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->resource->id,
            'shareWishId' => ToolsUserPrayerRepository::encrypt($this->resource),
            'birthday'    => $this->resource->birthday,
            'wish'        => $this->resource->wish,
            'pray'        => $this->resource->pray,
        ];
    }
}
