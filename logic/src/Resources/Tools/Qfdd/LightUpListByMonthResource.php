<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Tools\Qfdd;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class LightUpListByMonthResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => (int) ($this->resource->toolsUserPrayer ? $this->resource->toolsUserPrayer->id : 0),
            'lightsId'  => (int) ($this->resource->toolsUserPrayer ? $this->resource->toolsUserPrayer->tools_prayer_lights_id : 0),
            'name'      => (string) ($this->resource->toolsUserPrayer ? $this->resource->toolsUserPrayer->name : ''),
            'birthday'  => (string) ($this->resource->toolsUserPrayer ? $this->resource->toolsUserPrayer->birthday : ''),
            'wish'      => (string) ($this->resource->toolsUserPrayer ? $this->resource->toolsUserPrayer->wish : ''),
            'maxDays'   => $this->resource->max_days,
            'createdAt' => $this->resource->created_at->format('Y-m-d H:i:s'),
        ];
    }
}
