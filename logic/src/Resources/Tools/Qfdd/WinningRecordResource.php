<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Tools\Qfdd;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class WinningRecordResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'ranking'                    => $this->resource->ranking,
            'type'                       => $this->resource->type,
            'bonus'                      => $this->resource->bonus,
            'toolsUserPrayerLeaderboard' => [
                'number'    => $this->resource->toolsUserPrayerLeaderboard->number,
                'type'      => $this->resource->toolsUserPrayerLeaderboard->type,
                'typeText'  => $this->resource->toolsUserPrayerLeaderboard->type_text,
                'startedAt' => $this->resource->toolsUserPrayerLeaderboard->started_at->format('m.d'),
                'endedAt'   => $this->resource->toolsUserPrayerLeaderboard->ended_at->format('m.d'),
            ],
        ];
    }
}
