<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Tools\Qfdd;

use CalendarLogic\Repositories\ToolsUserPrayerRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ShareWishDetailResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'             => ToolsUserPrayerRepository::encrypt($this->resource),
            'lightsInfo'     => QfddLightsResource::make($this->resource->toolsPrayerLights),
        ];
    }
}
