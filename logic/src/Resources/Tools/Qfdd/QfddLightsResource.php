<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Tools\Qfdd;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class QfddLightsResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $effects = [];
        foreach ($this->resource->effect as $effect) {
            $effects[] = [
                'text' => $effect['text'],
                'img'  => res_url($effect['img']),
            ];
        }
        $isLight = 0;
        if ($this->resource->tools_user_prayeies_count) {
            $isLight = 1;
        }

        return [
            'id'          => $this->resource->id,
            'name'        => $this->resource->name,
            'description' => $this->resource->description,
            'crowd'       => $this->resource->crowd,
            'effect'      => $effects,
            'isLight'     => $isLight,
        ];
    }
}
