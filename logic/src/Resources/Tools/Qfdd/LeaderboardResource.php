<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Tools\Qfdd;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class LeaderboardResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'number'   => $this->resource->number,
            'type'     => $this->resource->type,
            'typeText' => $this->resource->type_text,
            'startedAt'=> $this->resource->started_at->format('m.d'),
            'endedAt'  => $this->resource->ended_at->format('m.d'),
        ];
    }
}
