<?php

namespace CalendarLogic\Resources\Tools\Qfdd;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class WinningReportDetailResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [];
        switch ($this->resource->type) {
            case 1:
                $data = $this->blessListByWeek();
                break;
            case 2:
                $data = $this->lightUpListByMonth();
                break;
            default:
                break;
        }
        return $data;
    }

    private function blessListByWeek()
    {
        $data = [];
        foreach (collect($this->resource->content)->take(3) as $item) {
            $data[] = [
                "name"          => $item['name'],
                "pray"          => $item['pray'],
                "wish"          => $item['wish'],
                "totalDay"      => $item['total_day'],
                "lastLightAt"   => $item['last_light_at'],
                "continuousDay" => $item['continuous_day'],
                "weekMaxCount"  => $item['week_max_count'],
                "lightsId"      => $item['tools_prayer_lights_id']
            ];
        }
        return $data;
    }


    private function lightUpListByMonth()
    {
        $data = [];
        foreach (collect($this->resource->content)->take(3) as $item) {
            $data[] = [
                "name"           => $item['tools_user_prayer']['name'],
                "pray"           => $item['tools_user_prayer']['pray'],
                "wish"           => $item['tools_user_prayer']['wish'],
                "totalDay"       => $item['tools_user_prayer']['total_day'],
                "lastLightAt"    => $item['tools_user_prayer']['last_light_at'],
                "continuousDay"  => $item['tools_user_prayer']['continuous_day'],
                "maxDays" => $item['max_days'],
                "lightsId"       => $item['tools_user_prayer']['tools_prayer_lights_id']
            ];
        }
        return $data;
    }
}
