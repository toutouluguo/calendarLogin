<?php

namespace CalendarLogic\Resources\Tools\Qfdd;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class WinningReportResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "number"           => $this->resource->number,
            "id"               => $this->resource->id,
            "startedTimestamp" => $this->resource->started_at->timestamp,
            "endedTimestamp"   => $this->resource->ended_at->timestamp,
            "startedStr"       => $this->resource->started_at->format("m.d"),
            "endedStr"         => $this->resource->ended_at->format("m.d")
        ];
    }
}
