<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Tools\Qfdd;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BlessListByWeekResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->resource->id,
            'lightsId'  => $this->resource->tools_prayer_lights_id,
            'name'      => $this->resource->name,
            'birthday'  => $this->resource->birthday,
            'wish'      => $this->resource->wish,
            'totalPray' => $this->resource->pray,
            'weekPray'  => $this->resource->week_max_count,
        ];
    }
}
