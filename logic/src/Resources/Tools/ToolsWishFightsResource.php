<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Tools;

use Illuminate\Http\Resources\Json\JsonResource;

class ToolsWishFightsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->resource->id,
            'name'      => $this->resource->name,
            'wish'      => $this->resource->wish,
            'img'       => res_url($this->resource->img),
            'light'     => res_url('tools/xydd/light/' . $this->resource->light),
            'pray'      => $this->resource->pray,
            'createdAt' => $this->resource->created_at->format('Y.m.d'),
        ];
    }
}
