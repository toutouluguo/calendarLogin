<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Tools;

use CalendarLogic\Repositories\AppRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SettingResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $appRepository = new AppRepository();

        return [
            'feedback' => (int) ($appRepository->getAppFeedbackControlBYChannelAndVersion() ? 1 : 0),
        ];
    }
}
