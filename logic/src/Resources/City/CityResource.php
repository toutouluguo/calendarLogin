<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\City;

use CalendarLogic\Models\ChinaCity;
use Illuminate\Http\Resources\Json\JsonResource;

class CityResource extends JsonResource
{
    /**
     * @var ChinaCity
     */
    public $resource;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'adCode'         => $this->resource->adcode,
            //            'adm1NameZh'     => $this->resource->adm1_name_zh,
            //            'adm2NameZh'     => $this->resource->adm2_name_zh,
            //            'locationNameZh' => $this->resource->location_name_zh,
            //            'latitude'       => $this->resource->latitude,
            //            'longitude'      => $this->resource->longitude,
            //            'area'           => sprintf('%s-%s', $this->resource->adm2_name_zh, $this->resource->location_name_zh),
            'area'           => $this->resource->adm2_name_zh,
            'location'       => "{$this->resource->longitude},{$this->resource->latitude}",
        ];
    }
}
