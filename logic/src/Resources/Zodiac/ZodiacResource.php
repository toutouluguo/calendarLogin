<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Zodiac;

use Illuminate\Http\Resources\Json\JsonResource;

class ZodiacResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type'         => $this->resource->type,
            'monthFortune' => $this->resource->month_fortune,
            'summary'      => $this->resource->summary,
            'career'       => $this->resource->career,
            'love'         => $this->resource->love,
            'healthy'      => $this->resource->healthy,
            'wealth'       => $this->resource->wealth,
        ];
    }
}
