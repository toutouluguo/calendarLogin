<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Lunar;

use com\nlf\calendar\Lunar;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FestivalDetailResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        if (2 == $this->resource->festival_category_id) {
            $content                  = json_decode($this->resource->content, true);
            $content['backgroundImg'] = res_url($content['backgroundImg']);
            $time                     = Lunar::fromDate(new \DateTime())->getJieQiTable()[$content['name']];
            $content['time']          = sprintf('%s月%s日%s:%s', $time->getMonth(), $time->getDay(), $time->getHour(), $time->getMinute());
            $type                     = 2;
        } else {
            $content = $this->resource->content;
            $type    = 1;
        }

        return [
            'id'      => $this->resource->id,
            'type'    => $type,
            'name'    => $this->resource['name'],
            'content' => $content,
        ];
    }
}
