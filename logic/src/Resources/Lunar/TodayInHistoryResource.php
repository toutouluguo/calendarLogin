<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Lunar;

use com\nlf\calendar\Lunar;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TodayInHistoryResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $data['img']  = res_url('todayInHistory/default.jpg');
        $data['date'] = now()->format('Y年m月d日');
        $data['week'] = '星期' . Lunar::fromDate(now())->getWeekInChinese();
        foreach ($this->resource->history as $key => $item) {
            $data[$key] = $item;
        }

        return $data;
    }
}
