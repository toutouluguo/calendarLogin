<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Constellation;

use Illuminate\Http\Resources\Json\JsonResource;

class ConstellationPairingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'male'         => $this->resource->male,
            'female'       => $this->resource->female,
            'idx'          => $this->resource->idx,
            'proportion'   => $this->resource->proportion,
            'affectionate' => $this->resource->affectionate,
            'everlasting'  => $this->resource->everlasting,
            'result'       => $this->resource->result,
            'advice'       => $this->resource->advice,
            'precaution'   => $this->resource->precaution,
        ];
    }
}
