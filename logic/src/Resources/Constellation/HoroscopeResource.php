<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Constellation;

use CalendarLogic\Models\Horoscope;
use Illuminate\Http\Resources\Json\JsonResource;

class HoroscopeResource extends JsonResource
{
    /**
     * @var Horoscope
     */
    public $resource;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $config = config("horoscopes.constellation.{$this->resource->constellation}");

        return [
            'name'         => $config['name'] ?? '',
            'imageUrl'     => res_url("constellation/{$this->resource->constellation}.png"),
            'dateDesc'     => $config['date'] ?? '',
            'fortuneScore' => $this->resource->fortune_score,
            'fortuneDesc'  => $this->resource->fortune_desc,
            'evaluateData' => $this->resource->evaluate_data,
        ];
    }
}
