<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\LearnPhilosophy;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class LearnPhilosophyBottomToolsResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title'   => $this->resource['title'] ?? '',
            'img'     => $this->resource['img'] ? res_url($this->resource['img']) : '',
            'toolUrl' => $this->resource['url'] ?? '',
        ];
    }
}
