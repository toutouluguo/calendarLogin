<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\LearnPhilosophy;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AlbumShowResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->resource->id,
            'name'             => $this->resource->name,
            'cover'            => res_url($this->resource->cover),
            'introduce'        => $this->resource->introduce,
            'learnPhilosophies'=> LearnPhilosophySimpleResource::collection($this->resource->learnPhilosophies),
        ];
    }
}
