<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\LearnPhilosophy;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SectionShowResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'        => $this->resource->name,
            'bottomTools' => LearnPhilosophyBottomToolsResource::collection($this->resource->bottom_tools),
            'albums'      => AlbumShowResource::collection($this->resource->LearnPhilosophyAlbums),
        ];
    }
}
