<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\LearnPhilosophy;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AlbumResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'        => $this->resource->name,
            'cover'       => res_url($this->resource->cover),
            'introduce'   => $this->resource->introduce,
            'bottomTools' => LearnPhilosophyBottomToolsResource::collection($this->resource->learnPhilosophyCategory->bottom_tools),
        ];
    }
}
