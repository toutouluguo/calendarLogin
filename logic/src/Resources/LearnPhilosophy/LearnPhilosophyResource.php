<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\LearnPhilosophy;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class LearnPhilosophyResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [

        ];
    }
}
