<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\LearnPhilosophy;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AlbumContentResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,

            'title'       => $this->resource->title,
            'type'        => $this->resource->type,
            'sourceUrl'   => $this->resource->source_url,
            'sourceTitle' => $this->resource->source_title,
            'pageViews'   => $this->resource->page_views,
            'releasedAt'  => $this->resource->released_at,
            'url'         => (string) sprintf('%ssubject/%s/%d', config('app.tool_h5_url'), $this->resource->learnPhilosophyAlbum->learnPhilosophyCategory->label, $this->resource->id),
        ];
    }
}
