<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\LearnPhilosophy;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ContentResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->resource->id,
            'content'     => $this->resource->content,
            'title'       => $this->resource->title,
            'authorName'  => $this->resource->author_name,
            'type'        => $this->resource->type,
            'sourceUrl'   => $this->resource->source_url,
            'sourceTitle' => $this->resource->source_title,
            'pageViews'   => $this->resource->page_views,
            'releasedAt'  => $this->resource->released_at,
//            'bottomTools' => LearnPhilosophyBottomToolsResource::collection($this->resource->learnPhilosophyAlbum->learnPhilosophyCategory->bottom_tools),
        ];
    }
}
