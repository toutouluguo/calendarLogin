<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Weather;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class WeatherNowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $weatherNow = $this->resource['weatherNow']['now'];
        $airNow     = $this->resource['airNow']['now'];
        $location   = $this->resource['location']['location'][0];

        return [
            'text'     => $location['adm2'] . '/' . $weatherNow['text'] . '/' . Str::replace('污染', '', $airNow['category']),
            'temp'     => $weatherNow['temp'],
            'icon'     => res_url('weather/' . $weatherNow['icon'] . '.png'),
        ];
    }
}
