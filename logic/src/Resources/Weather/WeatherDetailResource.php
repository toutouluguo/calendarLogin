<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Weather;

use Carbon\Carbon;
use com\nlf\calendar\Lunar;
use Illuminate\Http\Resources\Json\JsonResource;

class WeatherDetailResource extends JsonResource
{
    /**
     * @var array
     */
    public $resource;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // 四季
//        $season = match (now()->month) {
//            1, 2, 3 => 'spring',
//            4, 5, 6 => 'summer',
//            7, 8, 9 => 'autumn',
//            10, 11, 12 => 'winter',
//        };

        $lunar   = Lunar::fromDate(now());
        $jieqi   = $lunar->getJieQiTable();
        $chunfen = Carbon::parse($jieqi['春分']->getCalendar()->format('Y-m-d'));
        $xiazhi  = Carbon::parse($jieqi['夏至']->getCalendar()->format('Y-m-d'));
        $qiufen  = Carbon::parse($jieqi['秋分']->getCalendar()->format('Y-m-d'));
        $dongzhi = Carbon::parse($jieqi['冬至']->getCalendar()->format('Y-m-d'));
        switch ($nowTime = now()) {
            case $nowTime->gte($chunfen) && $nowTime->lt($xiazhi):
                $season = 'spring';
                break;
            case $nowTime->gte($xiazhi) && $nowTime->lt($qiufen):
                $season = 'summer';
                break;
            case $nowTime->gte($qiufen) && $nowTime->lt($dongzhi):
                $season = 'autumn';
                break;
            default:
                $season = 'winter';
        }

        $airLevel = [
            '优'    => 1,
            '良'    => 2,
            '轻度污染' => 3,
            '中度污染' => 4,
            '重度污染' => 5,
            '严重污染' => 6,
        ];
        $now         = $this->resource['weatherNow']            ?? [];
        $hourly      = $this->resource['weatherHourlyForecast'] ?? [];
        $everyDay    = $this->resource['weatherDailyForecast']  ?? [];
        $livingIndex = $this->resource['indicesInfo']           ?? [];
        $air         = $this->resource['airNow']                ?? [];
        $airDaily    = $this->resource['airDailyForecast']      ?? [];

        $data['weatherBackground'] = res_url("seasons/{$season}.png");
        $data['backgroundTabBar']  = res_url("seasons/{$season}_tabbar.png");
        // 今天温度
        $data['temp'] = $now['now']['temp'];
        // 当前天气状况
        $data['status'] = $now['now']['text'];
        // 预计未来2小时天气状况 @todo
        $data['expectedStatus'] = (
            ($now['now']['icon'] >= 300 && $now['now']['icon'] < 400)
            ||
            ($hourly['hourly'][0]['icon'] >= 300 && $hourly['hourly'][0]['icon'] < 400)
            ||
            ($hourly['hourly'][1]['icon'] >= 300 && $hourly['hourly'][1]['icon'] < 400)
        ) ? '2小时内有降雨' : '2小时内无降雨';
        // 体感温度
        $data['feelsLike'] = $now['now']['feelsLike'];
        // 湿度
        $data['humidity'] = $now['now']['humidity'];
        // 风向
        $data['windDir'] = $now['now']['windDir'];
        // 风向等级(1、3-4)
        $data['windScale'] = $now['now']['windScale'];
        $icon              = (int) $now['now']['icon'];
        // 雨量 （0无雨、1有雨）
        $data['rainfall'] =
            ($everyDay['daily'][0]['iconDay'] >= 300 && $everyDay['daily'][0]['iconDay'] < 400)
            ||
            ($everyDay['daily'][0]['iconNight'] >= 300 && $everyDay['daily'][0]['iconNight'] < 400)
                ? 1 : 0;
        // 空气质量指数
        $data['aqi'] = $air['now']['aqi'];
        // 当前空气质量状况
        $data['aqiCategory'] = $air['now']['category'];
        // 日落时间
        $data['sunsetTime'] = $everyDay['daily'][0]['sunset'];
        // 日出时间
        $data['sunriseTime'] = $everyDay['daily'][0]['sunrise'];
        // 今天最低到最高温度
        $data['todayTemp'] = $everyDay['daily'][0]['tempMin'] . '/' . $everyDay['daily'][0]['tempMax'];

        // 今天空气质量指数等级(1优、2良、3轻度污染、4中度污染、5重度污染、6严重污染)
        $data['todayAqiLevel'] = $airLevel[$airDaily['daily'][0]['category']];
        // 今天天气状况
        $data['todayStatus'] = $everyDay['daily'][0]['textDay'] === $everyDay['daily'][0]['textNight']
            ? $everyDay['daily'][0]['textDay']
            : $everyDay['daily'][0]['textDay'] . '转' . $everyDay['daily'][0]['textNight'];
        $data['tomorrowTemp'] = $everyDay['daily'][1]['tempMin'] . '/' . $everyDay['daily'][1]['tempMax']; // /明天最低到最高温度
        // 明天空气质量 TODO

        // 明天空气质量指数等级(1优、2良、3轻度污染、4中度污染、5重度污染、6严重污染
        $data['tomorrowAqiLevel'] = $airLevel[$airDaily['daily'][1]['category']];
        // 明天天气状况
        $data['tomorrowStatus'] = $everyDay['daily'][1]['textDay'] === $everyDay['daily'][1]['textNight']
            ? $everyDay['daily'][1]['textDay']
            : $everyDay['daily'][1]['textDay'] . '转' . $everyDay['daily'][1]['textNight'];

        // 每小时天气
        $data['hourly'][] = [
            // 时间（1点、2点、3点）
            'fxTime'    => '现在',
            // 温度
            'temp'      => $now['now']['temp'],
            // 小图片URL（多云/下雨/下雪）
            'icon'      => $now['now']['icon'],
            'status'    => $now['now']['text'],
            // 风向
            'windDir'   => $now['now']['windDir'],
            // 风向等级(1、3-4)
            'windScale' => $now['now']['windScale'],
        ];
        foreach ($hourly['hourly'] as $k => $v) {
            $data['hourly'][] = [
                // 时间（1点、2点、3点）
                'fxTime'    => \Illuminate\Support\Carbon::parse($v['fxTime'])->format('H点'),
                // 温度
                'temp'      => $v['temp'],
                // 小图片URL（多云/下雨/下雪）
                'icon'      => $v['icon'],
                'status'    => $v['text'],
                // 风向
                'windDir'   => $v['windDir'],
                // 风向等级(1、3-4)
                'windScale' => $v['windScale'],
            ];
        }

        $data['everyDay'] = [];
        foreach ($everyDay['daily'] as $key => $item) {
            $ariKey             = min($key, 4);
            $data['everyDay'][] = [
                'fxDate'      => Carbon::parse($item['fxDate'])->format('m/d'),
                'fxTime'      => 0 == $key ? '今天' : date_week_name(Carbon::parse($item['fxDate'])->dayOfWeek),
                'tt'          => Carbon::createFromTimestamp(strtotime($item['fxDate']))->isoFormat('MMM'),
                'tempMin'     => $item['tempMin'],
                'tempMax'     => $item['tempMax'],
                'tempMinIcon' => $item['iconNight'],
                'tempMaxIcon' => $item['iconDay'],
                // 明天空气质量指数等级(1优、2良、3轻度污染、4中度污染、5重度污染、6严重污染) todo
                'aqiLevel'    => $airLevel[$airDaily['daily'][$ariKey]['category']],
            ];
        }

        $data['livingIndex'] = [];
        foreach ($livingIndex['daily'] as $item) {
            $data['livingIndex'][] = [
                'icon'     => '',
                'name'     => 10 == $item['type'] ? '空气指数' : $item['name'],
                'category' => $item['category'],
            ];
        }

        return $data;
    }
}
