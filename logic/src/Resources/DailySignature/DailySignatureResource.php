<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\DailySignature;

use Illuminate\Http\Resources\Json\JsonResource;

class DailySignatureResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name'     => $this->resource->name,
            'sign'     => $this->resource->sign,
            'level'    => $this->resource->level,
            'poetry'   => $this->resource->poetry,
            'analysis' => $this->resource->analysis,
            'essence'  => $this->resource->essence,
            'content'  => $this->resource->content,
        ];
    }
}
