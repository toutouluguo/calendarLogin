<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class BaseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $birthday = '';
        if ($this->resource->birthday) {
            $birthday = Carbon::parse($this->resource->birthday)->toDateString();
        }

        return [
            'phone'                 => (string)$this->resource->phone ? substr_replace($this->resource->phone, '****', 3, 4) : '',
            'name'                  => (string)$this->resource->name,
            'avatar'                => (string)res_url($this->resource->avatar),
            'isSetPassword'         => (int)$this->resource->is_set_password,
            'gender'                => (int)$this->resource->gender,
            'birthday'              => (string)$birthday,
            'birthdayHour'          => (int)$this->resource->birthday_hour,
            'personalizedSignature' => (string)$this->resource->personalized_signature,
            'shareName'             => (string)$this->resource->share_name,
        ];
    }
}
