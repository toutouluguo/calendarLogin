<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Almanac;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class IllustrationResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $dimujing = [];

        foreach ($this->resource['dimujing'] as $item) {
            $dimujing[] = [
                'year'   => $item->year,
                'ganZhi' => $item->gan_zhi,
                'img'    => res_url($item->dimujing),
            ];
        }

        return [
            'centenary' => $this->resource['centenary'] ?? '',
            'springCow' => $this->resource['illustration']->spring_cow ? res_url($this->resource['illustration']->spring_cow) : '',
            'dimujing'  => $dimujing,
            'position'  => $this->resource['illustration']->position ? res_url($this->resource['illustration']->position) : '',
        ];
    }
}
