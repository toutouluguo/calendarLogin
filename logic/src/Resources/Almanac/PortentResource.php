<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Resources\Almanac;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PortentResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'happened' => $this->resource->happened,
            'portent'  => $this->resource->portent,
            'solve'    => $this->resource->solve,
        ];
    }
}
