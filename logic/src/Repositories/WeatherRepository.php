<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Foundation\Weather\QwApi;
use CalendarLogic\Resources\Weather\WeatherDetailResource;
use CalendarLogic\Resources\Weather\WeatherNowResource;
use CalendarLogic\Models\ChinaCity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class WeatherRepository
{
    private static int|float $TTL = 20 * 60;

    public function hotCity(int $number = 20, string $range = 'cn')
    {
        return Cache::tags('weather')->remember($this->getCacheName($range, 'hotCity'), self::$TTL, function () use ($number, $range) {
            $cities = QwApi::hotCity($number, $range);

            if (empty($cities)) {
                return null;
            }

            return ChinaCity::query()
                ->whereIn('location_id', collect($cities)->pluck('id'))
                ->groupBy('adm2_name_zh')
                ->get([
                    'adcode',
                    'adm1_name_zh',
                    'adm2_name_zh',
                    'location_name_zh',
                    'latitude',
                    'longitude',
                ]);
        }) ?? [];
    }

    public function detailByLongitudeAndLatitude($location, int $forecastDay = 7, int $forecastHour = 24, int $indicesDay = 1, string $indicesType = '3,1,2,4,9,15,6,10,16,13,5,14'): array
    {
        return QwApi::detail($location, $forecastDay, $forecastHour, $indicesDay, $indicesType);
    }

    public function getDetailByLongitudeAndLatitudeByCache(Request $request)
    {
        $location = $this->getLocation($request);

        return Cache::tags('weather')->remember($this->getCacheName($location, 'detailByLongitudeAndLatitude'), self::$TTL, function () use ($location, $request) {
            $resource = $this->detailByLongitudeAndLatitude($location);
            if (isset($resource['weatherNow']['code']) && 200 == $resource['weatherNow']['code']
                && isset($resource['airNow']['code']) && 200 == $resource['airNow']['code']
                && isset($resource['airDailyForecast']['code']) && 200 == $resource['airDailyForecast']['code']
                && isset($resource['weatherDailyForecast']['code']) && 200 == $resource['weatherDailyForecast']['code']
                && isset($resource['weatherHourlyForecast']['code']) && 200 == $resource['weatherHourlyForecast']['code']
                && isset($resource['indicesInfo']['code']) && 200 == $resource['indicesInfo']['code']
            ) {
                return WeatherDetailResource::make($resource)->toArray($request);
            }

            return null;
        }) ?? [];
    }

    public function detailByAdcode(string $adcode, int $forecastDay = 7, int $forecastHour = 24, int $indicesDay = 1, string $indicesType = '3,1,2,4,9,15,6,10,16,13,5,14'): array
    {
        $city = ChinaCity::query()->where('adcode', $adcode)->first();
        if (!$city) {
            return [];
        }

        return QwApi::detail($city->longitude, $city->latitude, $forecastDay, $forecastHour, $indicesDay, $indicesType);
    }

    /**
     * 当前天气情况.
     *
     * @return array
     */
    public function getNow($location)
    {
        return QwApi::weatherAndAirNow($location);
    }

    public function getNowByCache(Request $request)
    {
        $location = $this->getLocation($request);

        return Cache::tags('weather')->remember($this->getCacheName($location, 'getNow'), self::$TTL, function () use ($location, $request) {
            $resource = $this->getNow($location);
            if (isset($resource['airNow']) && 200 == $resource['airNow']['code']
                && isset($resource['weatherNow']) && 200 == $resource['weatherNow']['code']
                && isset($resource['location']) && 200 == $resource['location']['code']
            ) {
                return WeatherNowResource::make($resource)->toArray($request);
            }

            return null;
        }) ?? [];
    }

    public function getCacheName($location, $name)
    {
        $city = self::getCityLookUp($location);
        if ('200' == $city['code']) {
            return 'Weather:' . $name . ':' . $city['location'][0]['id'];
        }

        return 'Weather:' . $name . ':' . $location;
    }

    public static function getCityLookUp(string $location)
    {
        return Cache::tags('weather')->remember('weather:' . $location, now()->addDay(), function () use ($location) {
            return QwApi::cityLookUp($location);
        });
    }

    public function getLocation(Request $request)
    {
        return $request->get('adcode', '101010700');
    }
}
