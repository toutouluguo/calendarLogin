<?php

declare(strict_types=1);

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\AppChannel;
use CalendarLogic\Models\AppVersion;
use CalendarLogic\Models\UserAppActivity;
use Illuminate\Http\Request;

class UserAppActivityRepository
{
    /**
     * 打开APP次数增加.
     *
     * @return
     */
    public function increment(Request $request)
    {
        $versionId = AppVersion::query()->where('version_name', $request->header('version'))->value('id');
        $channelId = AppChannel::query()->where('channel', $request->header('channelName'))->value('id');
        if (!$versionId || !$channelId) {
            return true;
        }

        return UserAppActivity::query()
            ->firstOrCreate([
                'client'         => $request->header('imei'),
                'app_version_id' => $versionId,
                'app_channel_id' => $channelId,
            ], ['num' => 0])->increment('num');
    }

    /**
     * 获取访问次数.
     *
     * @return mixed
     */
    public function getNum(Request $request)
    {
        return (int) UserAppActivity::query()
            ->whereHas('appVersion', function ($query) use ($request) {
                $query->where('version_name', $request->header('version'));
            })
            ->whereHas('appChannel', function ($query) use ($request) {
                $query->where('channel', $request->header('channelName'));
            })
            ->where('client', $request->header('imei'))
            ->value('num');
    }
}
