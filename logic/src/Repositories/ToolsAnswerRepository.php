<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\ToolsAnswer;

class ToolsAnswerRepository
{
    public function getRandomOne()
    {
        return ToolsAnswer::query()->inRandomOrder()->first();
    }
}
