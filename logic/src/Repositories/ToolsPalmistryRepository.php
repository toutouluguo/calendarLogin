<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\ToolsPalmistry;

class ToolsPalmistryRepository
{
    /**
     * 获取手相.
     *
     * @return \App\Models\BaseModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function get()
    {
        return ToolsPalmistry::query()->select([
            'love',
            'marriage',
            'wisdom',
            'career',
            'life',
        ])->first();
    }
}
