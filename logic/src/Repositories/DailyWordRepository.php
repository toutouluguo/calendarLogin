<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\DailyWord;

class DailyWordRepository
{
    protected static array $baseColumn = [
        'id',
        'word',
        'thumb',
        'show_date',
        'img',
    ];

    /**
     * 新闻列表.
     *
     * @param $category
     * @param array
     *
     * @return
     */
    public function getList(int $page = 1, int $perPage = 10, $column = []): array|\Illuminate\Database\Eloquent\Collection
    {
        $column = !empty($column) ? array_merge($column, self::$baseColumn) : self::$baseColumn;

        return DailyWord::query()
            ->where('show_date', '<=', now())
            ->select($column)
            ->orderByDesc('show_date')
            ->limit($perPage)
            ->offset(($page - 1) * $perPage)
            ->get();
    }
}
