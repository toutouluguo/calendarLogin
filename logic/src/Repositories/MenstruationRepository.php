<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\Menstruation;

class MenstruationRepository
{
    /**
     * 用户经期数据.
     *
     * @return \App\Models\BaseModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function getFromUserId(int $id)
    {
        return Menstruation::query()->where('user_id', $id)->first();
    }

    /**
     * 保存用户经期数据.
     *
     * @return \App\Models\BaseModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function saveUserData(int $id, array $data)
    {
        return Menstruation::query()->updateOrCreate(['user_id' => $id], ['data' => $data]);
    }
}
