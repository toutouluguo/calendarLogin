<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Enums\ApiResponseConst;
use CalendarLogic\Models\ChinaCity;

class CityRepository
{
    /**
     * 城市搜索.
     *
     * @return ChinaCity[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    public function search(string $keyWord, int $takeCount = ApiResponseConst::PER_PAGE)
    {
        return ChinaCity::query()
            ->whereFullText([
                'location_name_en',
                'location_name_zh',
                'adm1_name_zh',
                'adm1_name_en',
            ], "'+$keyWord'", ['mode' => 'boolean'])
            ->take($takeCount)
            ->get([
                'adcode',
                'adm1_name_zh',
                'adm2_name_zh',
                'location_name_zh',
                'latitude',
                'longitude',
            ]);
    }
}
