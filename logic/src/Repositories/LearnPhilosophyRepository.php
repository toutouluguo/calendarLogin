<?php

declare(strict_types=1);

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\LearnPhilosophy;
use CalendarLogic\Models\LearnPhilosophyView;

class LearnPhilosophyRepository
{
    /**
     * 专辑内容.
     */
    public function getByAlbumId(int $album_id, int $page = 1, int $perPage = 20): array|\Illuminate\Database\Eloquent\Collection
    {
        if ($perPage > 60) {
            $perPage = 60;
        }

        return LearnPhilosophy::with(['learnPhilosophyAlbum' => function ($query) {
            return $query->select('id', 'learn_philosophy_category_id')
                ->with(['learnPhilosophyCategory' => function ($query) {
                    $query->select('id', 'label');
                }]);
        }])
            ->where('learn_philosophy_album_id', $album_id)
            ->limit($perPage)
            ->orderByDesc('sort')
            ->orderByDesc('id')
            ->select(['id', 'learn_philosophy_album_id', 'title', 'type', 'source_url', 'source_title', 'released_at', 'page_views'])
            ->offset(($page - 1) * $perPage)
            ->get();
    }

    /**
     * 以前根据分类获取banner图用的.
     *
     * @return \App\Models\BaseModel|LearnPhilosophy
     */
    public function getByLabelAndId(string $label, int $id)
    {
        return LearnPhilosophy::with(['learnPhilosophyAlbum' => function ($query) use ($label) {
            return $query->with(['learnPhilosophyCategory' => function ($query) use ($label) {
                return $query->where('label', $label);
            }]);
        }])->where('id', $id)->first();
    }

    public function getById(int $id)
    {
        $learnPhilosophy = LearnPhilosophy::query()->where('id', $id)->first();
        $this->addRealView($learnPhilosophy);

        return $learnPhilosophy;
    }

    public function addRealView(LearnPhilosophy $learnPhilosophy)
    {
        if (!request()->header('imei')) {
            return true;
        }
        $learnPhilosophyView = LearnPhilosophyView::query()->where('learn_philosophy_id', $learnPhilosophy->id)->where('imei', request()->header('imei'))->first();
        if ($learnPhilosophyView) {
            return true;
        }
        LearnPhilosophyView::query()->updateOrCreate(['learn_philosophy_id' => $learnPhilosophy->id, 'imei' => request()->header('imei')]);
        $learnPhilosophy->increment('real_views');

        return true;
    }
}
