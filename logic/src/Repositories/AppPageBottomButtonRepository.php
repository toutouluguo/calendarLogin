<?php

declare(strict_types=1);

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\AppPageBottomButton;

class AppPageBottomButtonRepository
{
    /**
     * 根据id查询.
     *
     * @return \App\Models\BaseModel|\App\Models\BaseModel[]|\LaravelIdea\Helper\App\Models\_IH_BaseModel_C|null
     */
    public function getContentById(int $id)
    {
        return AppPageBottomButton::query()->where('id', $id)->value('content');
    }
}
