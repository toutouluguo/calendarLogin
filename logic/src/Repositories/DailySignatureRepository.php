<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Enums\CacheNameConst;
use CalendarLogic\Models\DailySignature;
use CalendarLogic\Models\UserDailySignature;
use Illuminate\Support\Facades\Cache;

class DailySignatureRepository
{
    /**
     * 随机一签.
     *
     * @return bool|DailySignature|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function getRandomOne(): \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|DailySignature|bool
    {
        if (Cache::tags('cache')->has($this->getTodaySignatureCacheName())) {
            return false;
        }
        $signature = DailySignature::query()->inRandomOrder()->first();
        $this->setTodaySignatureCache($signature->id);
        $this->saveDailySignature($signature);

        return $signature;
    }

    protected function saveDailySignature(DailySignature $dailySignature)
    {
        if (request()->user()) {
            UserDailySignature::query()->create([
                'user_id'            => request()->user()->id,
                'daily_signature_id' => $dailySignature->id,
            ]);
        }
    }

    protected function setTodaySignatureCache(int $id): bool
    {
        return Cache::tags('cache')->put($this->getTodaySignatureCacheName(), $id, 60 * 60 * 24);
    }

    protected function getTodaySignatureCacheName(): string
    {
        return CacheNameConst::DAILY_SIGNATURE . ':' . now()->format('Ymd') . ':' . request()->header('imei');
    }

    /**
     * 今日历史抽签.
     */
    public function todaySignature(): \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|DailySignature|null
    {
        $id = Cache::tags('cache')->get($this->getTodaySignatureCacheName(), 0);

        return DailySignature::query()->find($id);
    }
}
