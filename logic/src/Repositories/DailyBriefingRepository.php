<?php

declare(strict_types=1);

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\DailyBriefing;

class DailyBriefingRepository
{
    /**
     * 获取当日资讯日报.
     *
     * @return \App\Models\BaseModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getTodayData()
    {
        $carbon = now();
        // 每天4点后才更新
        if ($carbon->hour < 4) {
            $carbon = $carbon->subDay();
        }

        return DailyBriefing::query()->where('date', $carbon->format('Y-m-d'))->first();
    }
}
