<?php

declare(strict_types=1);

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\Festival;
use CalendarLogic\Models\FestivalCategory;
use Carbon\Carbon;
use com\nlf\calendar\Lunar;
use com\nlf\calendar\LunarMonth;
use com\nlf\calendar\SolarWeek;
use Exception;

class FestivalRepository
{
    public function getCategory()
    {
        return FestivalCategory::query()->orderByDesc('sort')->get();
    }

    /**
     * 节日助手数据.
     *
     * @throws Exception
     */
    public function makeFestival(int $category_id, string $startDate = '', int $num = 20): array
    {
        if (2 == $category_id) {
            return $this->getJieQi($startDate, $num);
        }

        if (empty($startDate)) {
            $startDate = now()->format('Y-m-d');
        }
        $festivals = Festival::query()->where('festival_category_id', $category_id)->get();

        if ($festivals->isEmpty()) {
            return [];
        }
        $solarFestival   = [];
        $lunarFestival   = [];
        $specialFestival = [];
        foreach ($festivals as $festival) {
            switch ($festival->type) {
                case 1: // 阳历
                    $solarFestival[] = ['category_id' => $festival->festival_category_id, 'id' => $festival->id, 'name' => $festival->name, 'month' => $festival->date_operation[$festival->type]['month'], 'day' => $festival->date_operation[$festival->type]['day']];
                    break;
                case 2: // 阴历
                    $lunarFestival[] = ['category_id' => $festival->festival_category_id, 'id' => $festival->id, 'name' => $festival->name, 'month' => $festival->date_operation[$festival->type]['month'], 'day' => $festival->date_operation[$festival->type]['day']];
                    break;
                case 3: // 特殊计算
                    $specialFestival[] = ['category_id' => $festival->festival_category_id, 'id' => $festival->id, 'name' => $festival->name, 'month' => $festival->date_operation[$festival->type]['month'], 'week' => $festival->date_operation[$festival->type]['week'], 'day' => $festival->date_operation[$festival->type]['day']];
                    break;
            }
        }
        $solarFestival   = collect($solarFestival);
        $lunarFestival   = collect($lunarFestival);
        $specialFestival = collect($specialFestival);

        $today = Carbon::today();
        $lunar = Lunar::fromDate(new \DateTime($startDate));
        $data  = [];
        while (count($data) < $num) {
            $solar  = $lunar->getSolar();
            $isLeap = LunarMonth::fromYm($lunar->getYear(), $lunar->getMonth())->isLeap();
            if ($festivals = $lunarFestival->where('day', $lunar->getDay())->all()) {
                foreach ($festivals as $festival) {
                    // 农历节日，所有月份重复不需要跳过闰月,否则跳过闰月
                    if (0 == $festival['month'] || (!$isLeap && $festival['month'] == $lunar->getMonth())) {
                        $festivalName = $festival['name'];
                        if (1 == $category_id) {
                            $festivalName = $lunar->getMonthInChinese() . '月' . $lunar->getDayInChinese();
                        }
                        $data[] = [
                            'type'       => 1,
                            'id'         => $festival['id'],
                            'categoryId' => $festival['category_id'],
                            'festival'   => $festivalName,
                            'date'       => $solar->getYear() . '年' . $solar->getMonth() . '月' . $solar->getDay() . '日',
                            'dateTime'   => $solar->toString(),
                            'countdown'  => $today->diffInDays(Carbon::parse($solar->toYmd()), false),
                            'year'       => $solar->getYear(),
                            'month'      => $solar->getMonth(),
                            'day'        => $solar->getDay(),
                        ];
                    }
                }
            }
            // 阳历节日
            if ($festivals = $solarFestival->where('day', $solar->getDay())->all()) {
                foreach ($festivals as $festival) {
                    if ($festival['month'] == $solar->getMonth()) {
                        $data[] = [
                            'type'       => 1,
                            'id'         => $festival['id'],
                            'categoryId' => $festival['category_id'],
                            'festival'   => $festival['name'],
                            'date'       => $solar->getYear() . '年' . $solar->getMonth() . '月' . $solar->getDay() . '日',
                            'dateTime'   => $solar->toString(),
                            'countdown'  => $today->diffInDays(Carbon::parse($solar->toYmd()), false),
                            'year'       => $solar->getYear(),
                            'month'      => $solar->getMonth(),
                            'day'        => $solar->getDay(),
                        ];
                    }
                }
            }
            // 特殊节日
            if ($festivals = $specialFestival->where('month', $solar->getMonth())->all()) {
                $week = SolarWeek::fromDate(new \DateTime($solar->toYmd()), 0);
                foreach ($festivals as $festival) {
                    if ($week->getIndex() == $festival['week'] && $solar->getWeek() == $festival['day']) {
                        $data[] = [
                            'type'       => 1,
                            'categoryId' => $festival['category_id'],
                            'id'         => $festival['id'],
                            'festival'   => $festival['name'],
                            'date'       => $solar->getYear() . '年' . $solar->getMonth() . '月' . $solar->getDay() . '日',
                            'dateTime'   => $solar->toString(),
                            'countdown'  => $today->diffInDays(Carbon::parse($solar->toYmd()), false),
                            'year'       => $solar->getYear(),
                            'month'      => $solar->getMonth(),
                            'day'        => $solar->getDay(),
                        ];
                    }
                }
            }
            $lunar = $lunar->next(1);
        }

        return ['data' => $data, 'startDate' => $lunar->getSolar()->toYmd()];
    }

    /**
     * 二十四节气.
     *
     * @return array
     *
     * @throws Exception
     */
    public function getJieQi(string $startDate = '', int $num = 20)
    {
        if (empty($startDate)) {
            $startDate = now()->format('Y-m-d');
        }
        $festivals = Festival::query()->where('festival_category_id', 2)->get(['id', 'festival_category_id', 'name']);
        $today     = Carbon::today();
        $lunar     = Lunar::fromDate(new \DateTime($startDate));
        $data      = [];
        while (count($data) < $num) {
            if ($jieqi = $lunar->getJieQi()) {
                $solar = $lunar->getSolar();

                $data[] = [
                    'type'       => 2,
                    'id'         => (int) $festivals->where('name', $jieqi)->value('id'),
                    'categoryId' => (int) $festivals->where('name', $jieqi)->value('festival_category_id'),
                    'festival'   => $jieqi,
                    'date'       => $solar->getYear() . '年' . $solar->getMonth() . '月' . $solar->getDay() . '日',
                    'dateTime'   => $solar->toString(),
                    'countdown'  => $today->diffInDays(Carbon::parse($solar->toYmd()), false),
                    'year'       => $solar->getYear(),
                    'month'      => $solar->getMonth(),
                    'day'        => $solar->getDay(),
                ];
            }
            $lunar = $lunar->next(1);
        }

        return ['data' => $data, 'startDate' => $lunar->getSolar()->toYmd()];
    }

    /**
     * 节日详情.
     *
     * @param $id
     *
     * @return \App\Models\BaseModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getById($id): \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel|null
    {
        return Festival::query()->where('id', $id)->first();
    }

    public function getByName(string $name)
    {
        return Festival::query()->where('name', $name)->first();
    }
}
