<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\ToolsJoke;

class ToolsJokeRepository
{
    protected static array $baseColumn = [
        'id',
        'joke',
    ];

    /**
     * 笑话列表.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getList(int $page = 1, int $perPage = 10, $column = [])
    {
        $column = !empty($column) ? array_merge($column, self::$baseColumn) : self::$baseColumn;

        $joke = ToolsJoke::query()
            ->select($column)->limit($perPage);
        if (1 == $page) {
            return $joke->inRandomOrder()->get();
        }

        return $joke->offset(($page - 1) * $perPage)
            ->get();
    }
}
