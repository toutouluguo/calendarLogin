<?php

declare(strict_types=1);

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Resources\Tools\Qfdd\WinningReportDetailResource;
use CalendarLogic\Models\ToolsUserPrayerLeaderboard;

class ToolsUserPrayerLeaderboardRepository
{
    /**
     * 当前期数.
     *
     * @return \App\Models\BaseModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getCurrent()
    {
        return ToolsUserPrayerLeaderboard::query()
            ->where('started_at', '<=', now())
            ->where('ended_at', '>=', now())
            ->get();
    }

    /**
     * 中奖记录
     * @param int $type
     * @return \App\Models\BaseModel[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\LaravelIdea\Helper\App\Models\_IH_BaseModel_C
     */
    public function getList(int $type, $page = 1, $perPage = 20)
    {
        if ($perPage > 60) {
            $perPage = 60;
        }

        if ($type == 1) {
            $time = today()->startOfWeek()->toDateTimeString();
        } else {
            $time = today()->startOfMonth()->toDateTimeString();
        }
        return ToolsUserPrayerLeaderboard::query()
            ->where('started_at', '<', $time)
            ->where("type", $type)
            ->forPage($page, $perPage)
            ->orderByDesc('id')
            ->whereJsonLength('content', '>', 0)
            ->select('number', 'id', 'started_at', 'ended_at')
            ->get();
    }

    /**
     * 获奖详情
     * @param int $id
     * @return mixed
     */
    public function getById(int $id)
    {
        return ToolsUserPrayerLeaderboard::query()->where('id', $id)->first();
    }
}
