<?php

declare(strict_types=1);

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\AlmanacPortent;

class AlmanacPortentRepository
{
    public function getAll()
    {
        return AlmanacPortent::all();
    }
}
