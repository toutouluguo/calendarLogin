<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Console\Commands\Crawler\ZhongHuaWanNianLi\CrawlerTodayInHistoryCommand;
use CalendarLogic\Foundation\Festival\Adapter\CountdownFestival;
use CalendarLogic\Foundation\Lunar\LunarTimeFondation;
use CalendarLogic\Resources\Festival\FestivalBannerResource;
use CalendarLogic\Models\Festival;
use CalendarLogic\Models\FestivalBanner;
use CalendarLogic\Models\TodayInHistory;
use Carbon\Carbon;
use com\nlf\calendar\Lunar;
use com\nlf\calendar\util\HolidayUtil;

class LunarRepository
{
    public function holidays(int $year): array
    {
        $holidays = HolidayUtil::getHolidaysByYear($year);
        $data     = [];

        foreach ($holidays as $holiday) {
            $data[] = [
                'solarDate' => str_replace('-', '', $holiday->getDay()),
                'holiday'   => $holiday->getName(),
                'work'      => (int) $holiday->isWork(),
            ];
        }

        return $data;
    }

    public function dayTimes(string $time)
    {
        $lunar     = Lunar::fromDate(new \DateTime($time));
        $data      = [];
        $yiJi      = collect(config('lunar.timeYiJi.timeYiJi'))->where('_Date', $lunar->getDayInGanZhi())->first();
        $lunarTime = new LunarTimeFondation($lunar->getSolar()->toYmd());
        foreach ($lunar->getTimes() as $keys => $item) {
            if (12 == $keys) {
                continue;
            }

            $time = $item->getMinHm() . '-' . $item->getMaxHm();

            if (0 == $keys) {
                $time = '23:00-00:59';
            }
            $comTop = sprintf(
                '%s时 %s 冲%s 煞%s',
                $item->getGanZhi(),
                $time,
                $item->getChongShengXiao(),
                $item->getSha()
            );
            $lunarTime->setHour($lunar->getHour());
            $comGods = sprintf(
                '喜神%s 财神%s 福神%s',
                $lunarTime->getXishen(),
                $lunarTime->getCaishen(),
                $lunarTime->getFushen()
            );

            $data[] = [
                'gan'  => $item->getZhi(),
                'luck' => $item->getTianShenLuck(),
                'top'  => $comTop,
                'gods' => $comGods,
                'yi'   => $yiJi['Yi' . $keys],
                'ji'   => $yiJi['Ji' . $keys],

            ];
        }

        return $data;
    }

    /**
     * 今天宜忌.
     *
     * @return array
     *
     * @throws \Exception
     */
    public function getToDayYiJi()
    {
        return $this->getDayYiJi('now');
    }

    /**
     * 每日宜忌.
     *
     * @throws \Exception
     */
    public function getDayYiJi($time)
    {
        $d = Lunar::fromDate(new \DateTime($time));
        // 宜
        $yi = $d->getDayYi();
        // 忌
        $ji = $d->getDayJi();

        return ['yi' => $yi, 'ji' => $ji];
    }

    /**
     * 倒数日.
     *
     * @return array
     */
    public function countdown()
    {
        $CountdownFestival = new CountdownFestival();
        $collect           = $CountdownFestival->getFestival();

        // 法定节假日
        $holiday = $collect->where('isHoliday', true)->sortBy(function ($value, $key) {
            return Carbon::parse($value['day'])->timestamp;
        })->where(function ($value, $key) {
            return Carbon::parse($value['day'])->gte(today());
        })->take(2)->toArray();
        // 默认节日
        $festival = $collect->where('isHoliday', false)->sortBy(function ($value, $key) {
            return Carbon::parse($value['day'])->timestamp;
        })
            ->where(function ($value, $key) {
                return Carbon::parse($value['day'])->gte(today());
            })->take(1)->toArray();
        if (count($holiday) < 2 || count($festival) < 1) {
            $CountdownFestival->getFestival(Carbon::create()->addYear()->format('Y'));
            $addHoliday = $collect->where('isHoliday', true)
                ->where(function ($value, $key) {
                    return Carbon::parse($value['day'])->gte(today());
                })->take(2 - count($holiday))->toArray();
            $festival   = $collect->where('isHoliday', false)
                ->where(function ($value, $key) {
                    return Carbon::parse($value['day'])->gte(today());
                })->take(1)->toArray();
            $holiday    = array_merge($holiday, $addHoliday);
        }

        $result          = collect(array_merge(array_values($holiday), array_values($festival)))->sortBy(function ($value, $key) {
            return Carbon::parse($value['day'])->timestamp;
        });
        $festivalContent = Festival::query()->whereIn('name', $result->pluck('festival'))->orWhere('name', '高考')->select('festival_category_id','name', 'id')->get();

        $result = $result->toArray();

        $result[] = $CountdownFestival->getGaokao();
        $result   = array_values($result);
        foreach ($result as &$item) {
           if( $content         = $festivalContent->where('name', $item['festival'])->first()){
                $content->type = $content->festival_category_id == 2 ? 2: 1;
                $content = $content->toArray();
            }
            $item['content'] = (array) $content;
        }

        $banner = [];
        if ($festivalBanner = FestivalBanner::query()->with('festival')->where('show_date', today())->first()) {
            $banner = FestivalBannerResource::make($festivalBanner)->toArray(request());
        }

        $bottom = [
            ['name' => '二十四节气', 'id' => 2],
            ['name' => '传统节日', 'id' => 4],
            ['name' => '国际节日', 'id' => 5],
        ];

        return [
            'festival'    => (array) $result,
            'newYearDiff' => (int) Carbon::today()->diffInDays(Carbon::create(null, 1, 1)->addYear(), false),
            'banner'      => (object) $banner,
            'bottom'      => (array) $bottom,
        ];
    }

    public function getTodayInHistory($date)
    {
        if ($history = TodayInHistory::query()->where('date', $date)->first()) {
            return $history;
        }

        return (new CrawlerTodayInHistoryCommand())->updateByDate($date);
    }
}
