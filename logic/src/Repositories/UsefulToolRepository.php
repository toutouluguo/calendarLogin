<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\UsefulTool;
use CalendarLogic\Models\UsefulToolCategory;

class UsefulToolRepository
{
    /**
     * 所有启用的工具分类.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getShowCategoryAll()
    {
        $categories = UsefulToolCategory::query()->where('status', 1)->orderByDesc('sort')->get();
        $categories->each(function (UsefulToolCategory $category) {
            $category->load(['usefulTools' => function ($query) use ($category) {
                return $query->with(['pageActivity', 'pageUrl'])->where('status', 1)->limit($category->show_count);
            }]);
        });

        return $categories;
    }

    /**
     * 所有启用的工具分类.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getShowCategoryAllByType(int $type)
    {
        $categories = UsefulToolCategory::query()->where('status', 1)->orderByDesc('sort')->get();
        $categories->each(function (UsefulToolCategory $category) use ($type) {
            $category->load(['usefulTools' => function ($query) use ($category, $type) {
                return $query->with(['pageActivity', 'pageUrl'])->where('status', 1)->where('type', $type)->limit($category->show_count);
            }]);
        });

        return $categories;
    }

    /**
     * 所有分类的工具.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllCategoryAndTools()
    {
        return UsefulToolCategory::query()->with(['usefulTools' => function ($query) {
            $query->where('status', 1);
        }])->where('status', 1)->orderByDesc('sort')->get();
    }

    public function getCategoryAndToolsByType(int $type)
    {
        return UsefulToolCategory::query()->withWhereHas('usefulTools', function ($query) use ($type) {
            return $query->with(['pageUrl', 'pageActivity'])->where('type', $type)->where('status', 1);
        })->where('status', 1)->orderByDesc('sort')->get();
    }

    /**
     * 根据分类id获取所有工具.
     *
     * @return \App\Models\BaseModel[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getShowToolByCategoryId(int $id)
    {
        return UsefulTool::query()->where('useful_tool_category_id', $id)->get();
    }

    public function getByIds($ids)
    {
        return UsefulTool::query()->whereIn('id', $ids)->get();
    }

    public function getExternalDomain()
    {
        $urls = UsefulTool::query()->where('useful_tool_category_id', 3)->pluck('url');
        $data = [];
        foreach ($urls as $url) {
            $data[] = parse_url($url)['host'];
        }

        return array_values(array_unique($data));
    }
}
