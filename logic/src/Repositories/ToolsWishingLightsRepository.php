<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\ToolsWishingLights;
use CalendarLogic\Models\ToolsWishingLightsPray;

class ToolsWishingLightsRepository
{
    public function getList($page, $perPage, $order = 'id desc')
    {
        return ToolsWishingLights::query()->where('show_square', 1)->orderByRaw($order)->limit($perPage)->offset(($page - 1) * $perPage)->get();
    }

    public function getMyList($page, $perPage, $order = 'id desc')
    {
        return ToolsWishingLights::query()->where('user_id', request()->user()->id)->orderByRaw($order)->limit($perPage)->offset(($page - 1) * $perPage)->get();
    }

    public function getListByThisWeek($page, $perPage, $order = 'pray desc')
    {
        return ToolsWishingLights::query()->orderByRaw($order)->where('show_square', 1)->limit($perPage)->offset(($page - 1) * $perPage)->get();
    }

    public function add($data)
    {
        return ToolsWishingLights::query()->create($data);
    }

    public function pray($id)
    {
        ToolsWishingLightsPray::query()->create([
            'user_id'                 => request()->user()->id,
            'tools_wishing_lights_id' => $id,
        ]);
        ToolsWishingLights::query()->where('id', $id)->increment('pray');

        return true;
    }

    public function getPray($id)
    {
        return ToolsWishingLightsPray::query()->where('tools_wishing_lights_id', $id)
            ->where('user_id', request()->user()->id)
            ->first();
    }
}
