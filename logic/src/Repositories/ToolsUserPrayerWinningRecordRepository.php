<?php

declare(strict_types=1);

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\BaseModel;
use CalendarLogic\Models\ToolsUserPrayerWinningRecord;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use LaravelIdea\Helper\App\Models\_IH_BaseModel_C;
use LaravelIdea\Helper\App\Models\_IH_BaseModel_QB;

class ToolsUserPrayerWinningRecordRepository
{
    /**
     * 用户未读中奖信息.
     *
     * @return BaseModel|Builder|Collection|_IH_BaseModel_C|_IH_BaseModel_QB
     */
    public function getNoticeNotReadByUserId(int $id)
    {
        $list = ToolsUserPrayerWinningRecord::query()
            ->where('notice_read', 0)
            ->where('user_id', $id)
            ->orderByDesc('created_at')
            ->get();
        $this->readNoticeByUserId($id);

        return $list;
    }

    /**
     * 用户阅读消息.
     *
     * @return bool|int
     */
    public function readNoticeByUserId(int $id)
    {
        return ToolsUserPrayerWinningRecord::query()->where('user_id', $id)->update(['notice_read' => 1]);
    }

    /**
     * 个人中奖记录
     * @param int $id
     * @return BaseModel[]|Builder[]|Collection|_IH_BaseModel_C|_IH_BaseModel_QB[]
     */
    public function getWinningRecordByUserId(int $id)
    {
        return ToolsUserPrayerWinningRecord::query()
            ->with(['toolsUserPrayerLeaderboard' => function ($query) {
                return $query->select('id', 'type', 'number', 'started_at', 'ended_at');
            }])
            ->where('user_id', $id)
            ->orderByDesc('created_at')
            ->get();
    }

}
