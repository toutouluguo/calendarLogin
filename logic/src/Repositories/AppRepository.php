<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Enums\CacheNameConst;
use CalendarLogic\Models\App;
use CalendarLogic\Models\AppSetting;
use CalendarLogic\Models\AppVersion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class AppRepository
{
    /**
     * app类型.
     *
     * @param string client
     *
     * @return \App\Models\BaseModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getAppByClient(string $client)
    {
        return App::query()->where('client', $client)->first();
    }

    /**
     * 最新版本.
     *
     * @return \App\Models\BaseModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getLatestAppVersionByAppId(int $id)
    {
        return AppVersion::query()->where('app_id', $id)->latest()->first();
    }

    public function getLatestAppVersionByClient(string $client, ?string $channelName = '')
    {
        if (null === $channelName) {
            $channelName = 'Android' == $client ? 'official' : 'appstore';
        }

        return AppVersion::query()->whereHas('app', function ($query) use ($client) {
            return $query->where('client', $client);
        })
            ->whereJsonContains('audit->' . $channelName . '->audit', 0)
            ->latest()->firstOrNew();
    }

    /**
     * app版本控制.
     *
     * @return \App\Models\BaseModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function getAppSettingByAppVersion(string $client, string $version)
    {
        return AppVersion::query()->whereHas('app', function ($query) use ($client) {
            $query->where('client', $client);
        })->where('version_name', $version)->latest()->firstOrNew();
    }

    /**
     * 是否审核模式.
     */
    public function getAppAudit(Request $request): int
    {
        $client      = $request->header('client');
        $version     = $request->header('version');
        $channelName = $request->header('channelName');
        $appVersion  = $this->getAppSettingByAppVersion($client, $version);

        $audit      = (int) isset($appVersion->audit[$channelName]) ? $appVersion->audit[$channelName]['audit']        ?? 0 : 0;
        $openAppNum = (int) isset($appVersion->audit[$channelName]) ? $appVersion->audit[$channelName]['open_app_num'] ?? 0 : 0;
        // 用户访问次数
        if ((new UserAppActivityRepository())->getNum($request) < $openAppNum) {
            $audit = 1;
        }

        return $audit;
    }

    /**
     * 返回首页链接展示.
     *
     * @return mixed
     */
    public function getAppHomeUrl()
    {
        if ($appSetting = Cache::tags('config')->get(CacheNameConst::APP_SETTING)) {
            return collect($appSetting['home_url']['url'])->where('isShow', 1)->all();
        }

        return collect(AppSetting::query()->latest()->value('home_url')['url'])->where('isShow', 1)->all();
    }

    /**
     * 返回首页自定义按钮.
     *
     * @return mixed
     */
    public function getAppHomeButton()
    {
        if ($appSetting = Cache::tags('config')->get(CacheNameConst::APP_SETTING)) {
            return collect($appSetting['home_url']['button'])->all();
        }

        return collect(AppSetting::query()->latest()->value('home_url')['button'])->all();
    }

    /**
     * 首页工具.
     *
     * @return \App\Models\BaseModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getAppHomeTools()
    {
        return Cache::tags('config')->rememberForever(CacheNameConst::APP_SETTING, function () {
            return AppSetting::query()->latest()->first()->toArray();
        })['home_tools'];
    }

    /**
     * 充值广告开关.
     *
     * @return mixed
     */
    public function getAppAds()
    {
        return Cache::tags('config')->rememberForever(CacheNameConst::APP_SETTING, function () {
            return AppSetting::query()->latest()->first()->toArray();
        })['app_ads'];
    }

    /**
     * 充值广告配置.
     *
     * @return mixed
     */
    public function getAppChargeAds()
    {
        return Cache::tags('config')->rememberForever(CacheNameConst::APP_SETTING, function () {
            return AppSetting::query()->latest()->first()->toArray();
        })['charge_ads'];
    }

    /**
     * 反馈弹窗总开关.
     *
     * @return mixed
     */
    public function getAppFeedbackControl()
    {
        return Cache::tags('config')->rememberForever(CacheNameConst::APP_SETTING, function () {
            return AppSetting::query()->latest()->first()->toArray();
        })['feedback_control'];
    }

    /**
     * 根据版本和渠道查询反馈弹窗.
     *
     * @return \App\Models\BaseModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function getAppFeedbackControlBYChannelAndVersion()
    {
        $client      = request()->header('client');
        $channelName = request()->header('channelName');
        $version     = request()->header('version');

        return AppVersion::query()->whereHas('app', function ($query) use ($client) {
            return $query->where('client', $client);
        })->where('version_name', $version)
            ->whereJsonContains('audit->' . $channelName . '->feedback', 1)
            ->first();
    }
}
