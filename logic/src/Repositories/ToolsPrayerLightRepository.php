<?php

declare(strict_types=1);

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\ToolsPrayerLights;

class ToolsPrayerLightRepository
{
    public function getAll()
    {
        return ToolsPrayerLights::query()->get();
    }

    public function getAllWithUser($user = null)
    {
        $lights = ToolsPrayerLights::query();
        if ($user) {
            $lights->withCount(['toolsUserPrayeies' => function ($query) use ($user) {
                $query->where('user_id', $user->id)->whereDate('last_light_at', now()->toDateString());
            }]);
        }

        return $lights->get();
    }
}
