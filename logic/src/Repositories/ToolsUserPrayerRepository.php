<?php

declare(strict_types=1);

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\ToolsPrayLightRecord;
use CalendarLogic\Models\ToolsUserPrayer;
use CalendarLogic\Models\ToolsUserPrayerBless;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ToolsUserPrayerRepository
{
    /**
     * 自动添加祈福数量.
     *
     * @return void
     */
    protected function autoPray($user_id)
    {
        $user_id = 1;
        Cache::tags('cache')->remember('autoPray:' . $user_id, Carbon::tomorrow(), function () use ($user_id) {
            foreach (ToolsUserPrayer::query()->showSquare()->where('user_id', $user_id)->where('updated_at', '<', today()->toDateTimeString())->get() as $toolsUserPrayer) {
                if ($toolsUserPrayer->updated_at->lt(Carbon::today())) {
                    $toolsUserPrayer->showSquare()->increment('pray', random_int(5, 15));
                }
            }
            return true;
        });
    }

    /**
     * 获取用户某个灯的祈福.
     */
    public function getMyPrayByLight(int $light_id, int $user_id, int $page = 1, int $perPage = 3): array|\Illuminate\Database\Eloquent\Collection
    {
        if ($perPage > 60) {
            $perPage = 60;
        }
        $this->autoPray($user_id);


        return ToolsUserPrayer::query()->where('tools_prayer_lights_id', $light_id)
            ->limit($perPage)
            ->offset(($page - 1) * $perPage)
            ->where('user_id', $user_id)
            ->orderByDesc('id')
            ->get();
    }

    /**
     * 我的祈福.
     *
     * @return \App\Models\BaseModel[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getMyPray(int $user_id, int $page = 1, int $perPage = 20): array|\Illuminate\Database\Eloquent\Collection
    {
        if ($perPage > 60) {
            $perPage = 60;
        }
        $this->autoPray($user_id);


        return ToolsUserPrayer::query()->where('user_id', $user_id)
            ->limit($perPage)
            ->offset(($page - 1) * $perPage)
            ->orderByDesc('id')
            ->get();
    }

    /**
     * 许愿
     *
     * @param $data
     *
     * @throws Exception
     */
    public function wish($data): \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel
    {
        if (ToolsUserPrayer::query()->where('user_id', $data['user_id'])->where('tools_prayer_lights_id', $data['tools_prayer_lights_id'])->count() >= 3) {
            abort(423, '每盏灯最多只能许3个愿望！');
        }

        $data['pray']            = 0;
        $data['continuous_day']  = 1;
        $data['total_day']       = 1;
        $data['auto_light_date'] = null;
        $data['last_light_at']   = now();

        $prayer = ToolsUserPrayer::query()->create($data);
        $prayer->toolsPrayLightRecords()->save(
            new ToolsPrayLightRecord(['user_id' => $data['user_id'], 'type' => 1, 'record_date' => now()->toDateString()])
        );

        return $prayer;
    }

    /**
     * 删除愿望.
     *
     * @param $id
     * @param $user_id
     *
     * @return bool|mixed|null
     */
    public function del($id, $user_id): mixed
    {
        return ToolsUserPrayer::query()->where('user_id', $user_id)->where('id', $id)->delete();
    }

    /**
     * 点亮许愿灯.
     *
     * @throws Exception
     */
    public function lightUp(int $user_id, int $id): bool
    {
        $prayer = ToolsUserPrayer::query()->where('user_id', $user_id)->where('id', $id)->first();

        if (!$prayer) {
            abort(423, '请选择要点亮的灯!');
        }
        if ($prayer->lightToday()) {
            abort(423, '今天已经点亮，无需再次点亮！');
        }

        DB::beginTransaction();
        try {
            $continuous_day = 1;

            // 连续点灯
            if ($prayer->last_light_at && Carbon::parse('yesterday')->toDateString() == $prayer->last_light_at->toDateString()) {
                $continuous_day = $prayer->continuous_day + 1;
            }

            // 连续30天
            if ($continuous_day >= 30) {
                $prayer->auto_light_date = Carbon::today()->addDays(90)->toDateString();
            }
            $prayer->continuous_day = $continuous_day;
            $prayer->total_day      = $prayer->total_day + 1;
            $prayer->last_light_at  = now();
            $prayer->save();
            $prayer->toolsPrayLightRecords()->save(
                new ToolsPrayLightRecord(['user_id' => $user_id, 'type' => 1, 'record_date' => now()->toDateString()])
            );
            DB::commit();

            return true;
        } catch (Exception $e) {
            Log::info($e);
            DB::rollback();
            abort(423, '网络错误，请重试！');
        }
    }

    /**
     * 许愿详情.
     *
     * @return \App\Models\BaseModel|\App\Models\BaseModel[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function wishDetail(int $user_id, int $id)
    {
        return ToolsUserPrayer::query()->where('user_id', $user_id)->where('id', $id)->first();
    }

    /**
     * 根据日期查询点亮个数.
     */
    public function lightUpCountByDate(int $user_id, string $date = ''): int
    {
        if (empty($date)) {
            $date = now()->toDateString();
        }

        return ToolsUserPrayer::query()->whereAtDate('last_light_at', $date)->where(['user_id' => $user_id])->count();
    }

    /**
     * 祈福.
     *
     * @param int $isShare 分享链接祈福 0否，1 是
     */
    public function pray(int $id, string $imei, int $user_id = 0, int $isShare = 0): bool|int
    {
        $prayer = ToolsUserPrayer::query()->where('id', $id)->first();
        if (!$prayer) {
            abort(423, '请先许一个愿望，才能祈福!');
        }
        if ($prayer->user_id == $user_id) {
            abort(423, '请分享出去，让朋友给你祈福！');
        }
        $bless = ToolsUserPrayerBless::query()->where('tools_user_prayer_id', $id)->whereAtDate('created_at', now()->toDateString());
        if (0 === $isShare) {
            $bless = $bless->where('user_id', $user_id);
        } else {
            $bless = $bless->where('imei', $imei);
        }
        if ($bless->first()) {
            abort(423, '今天已经为TA祈福过了，请明天再来！！');
        }
        ToolsUserPrayerBless::query()->create([
            'tools_user_prayer_id' => $id,
            'user_id'              => $user_id,
            'imei'                 => $imei,
            'ip'                   => request()->ip(),
            'is_share'             => $isShare,
        ]);
        $prayer->pray = $prayer->pray + 1;

        return $prayer->save();
    }

    /**
     * 祈福广场.
     *
     * @return mixed
     */
    public function getSquareList(int $page = 1, int $perPage = 20, string $order = 'time')
    {
        if ($perPage > 60) {
            $perPage = 60;
        }
        if ('time' == $order) {
            $order = 'created_at';
        }

        return ToolsUserPrayer::query()->showSquare()
            ->orderByDesc($order)
            ->limit($perPage)
            ->offset(($page - 1) * $perPage)
            ->get();
    }

    /**
     * 许愿分享详情.
     *
     * @return \App\Models\BaseModel|\App\Models\BaseModel[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function getShareWishDetail(string $shareInfo)
    {
        $shareInfo = self::decrypt($shareInfo);

        return ToolsUserPrayer::with('toolsPrayerLights')->where('user_id', $shareInfo['user_id'])->where('id', $shareInfo['id'])->first();
    }

    /**
     * 分享许愿祈福.
     *
     * @return bool|int
     *
     * @throws Exception
     */
    public function shareWishPray(string $imei, string $shareInfo)
    {
        $shareInfo = self::decrypt($shareInfo);
        if (empty($imei)) {
            return throw new Exception();
        }

        return $this->pray($shareInfo['id'], $imei, 0, 1);
    }

    /**
     * 续签.
     *
     * @return bool|void
     *
     * @throws Exception
     */
    public function continueLightUp(int $user_id, int $id)
    {
        $prayer = ToolsUserPrayer::query()->where('id', $id)->where('user_id', $user_id)->first();
        if (!$prayer) {
            abort(423, '请先许愿！');
        }
        if (!$prayer->last_light_at) {
            return $this->lightUp($user_id, $id);
        }
        if ($prayer->lightToday()) {
            abort(423, '今天已经点亮了！');
        }
        $diffInDays = Carbon::today()->diffInDays($prayer->last_light_at->toDateString());
        if ($diffInDays > 3) {
            abort(423, '超过了3天,请重新点亮！');
        }
        DB::beginTransaction();
        try {
            $last_light_at          = $prayer->last_light_at;
            $prayer->continuous_day = $prayer->continuous_day + $diffInDays;
            $prayer->total_day      = $prayer->total_day + $diffInDays;
            $prayer->last_light_at  = now();
            $prayer->save();
            $toolsPrayLightRecords = [];
            while (Carbon::today()->diffInDays($last_light_at->toDateString()) > 0) {
                $last_light_at           = $last_light_at->addDay();
                $toolsPrayLightRecords[] = new ToolsPrayLightRecord(['user_id' => $user_id, 'type' => 3, 'record_date' => $last_light_at]);
            }
            $prayer->toolsPrayLightRecords()->saveMany($toolsPrayLightRecords);
            DB::commit();

            return true;
        } catch (Exception $e) {
            Log::info($e);
            DB::rollBack();

            return false;
        }
    }

    /**
     * 首次使用的日期
     */
    public function firstUseDate(int $user_id): Carbon|null
    {
        return ToolsUserPrayer::withTrashed()->where('user_id', $user_id)->orderBy('created_at')->value('created_at');
    }

    /**
     * 祈福周列表.
     *
     * @return \App\Models\BaseModel[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\LaravelIdea\Helper\App\Models\_IH_BaseModel_C|\LaravelIdea\Helper\App\Models\_IH_ToolsUserPrayer_C|\LaravelIdea\Helper\App\Models\_IH_ToolsUserPrayer_QB[]|ToolsUserPrayer[]
     */
    public function getBlessListByWeek(int $page = 1, int $perPage = 20)
    {
        return $this->getBlessList($page, $perPage, 'week');
    }

    /**
     * 祈福列表.
     *
     * @return \App\Models\BaseModel[]|ToolsUserPrayer[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\LaravelIdea\Helper\App\Models\_IH_BaseModel_C|\LaravelIdea\Helper\App\Models\_IH_ToolsUserPrayer_C|\LaravelIdea\Helper\App\Models\_IH_ToolsUserPrayer_QB[]
     */
    public function getBlessList(int $page = 1, int $perPage = 20, string $timeType = 'week', string $date = '')
    {
        if ($perPage > 60) {
            $perPage = 60;
        }

        $listQuery = ToolsUserPrayer::query()
            ->withCount(['toolsUserPrayerBlesses' => function ($query) use ($timeType, $date) {
                !$date && $date = Carbon::today();

                return $query->blessByTimeType($timeType, $date);
            }])
            ->orderBy('tools_user_prayer_blesses_count', 'desc')
            ->showSquare();

        return ToolsUserPrayer::query()->fromSub($listQuery->getQuery(), 'a')
            ->withTrashed()
            ->whereNull('deleted_at')
            ->selectRaw('*,max(tools_user_prayer_blesses_count) week_max_count')
            ->groupBy('user_id')
            ->limit($perPage)
            ->offset(($page - 1) * $perPage)
            ->showSquare()
            ->get();
    }

    /**
     * 月连续点亮天数.
     *
     * @return \App\Models\BaseModel[]|ToolsUserPrayer[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\LaravelIdea\Helper\App\Models\_IH_BaseModel_C|\LaravelIdea\Helper\App\Models\_IH_ToolsUserPrayer_C|\LaravelIdea\Helper\App\Models\_IH_ToolsUserPrayer_QB[]
     */
    public function getLightUpListByMonth(int $page = 1, int $perPage = 20)
    {
        return $this->getLightUpList($page, $perPage);
    }

    /**
     * 连续点亮天数列表.
     *
     * @return \App\Models\BaseModel[]|ToolsUserPrayer[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\LaravelIdea\Helper\App\Models\_IH_BaseModel_C|\LaravelIdea\Helper\App\Models\_IH_ToolsUserPrayer_C|\LaravelIdea\Helper\App\Models\_IH_ToolsUserPrayer_QB[]
     */
    public function getLightUpList(int $page = 1, int $perPage = 20, string $timeType = 'month', string $date = '')
    {
        if ($perPage > 60) {
            $perPage = 60;
        }
        !$date && $date = Carbon::today();
        $continueGroupQuery = ToolsPrayLightRecord::query()
            ->where('type', 1)
            ->whereHas('toolsUserPrayer', fn($query) => $query->showSquare())
            ->selectRaw('*,date(record_date)-row_number() over(partition by tools_user_prayer_id ORDER BY record_date) days')
            ->lightUpByTimeType($timeType, $date);

        $dayQuery = ToolsPrayLightRecord::query()->selectRaw('*,count(days) count_days')->fromSub($continueGroupQuery->getQuery(), 'a')
            ->orderByDesc('count_days')
            ->groupByRaw('tools_user_prayer_id,days');

        return ToolsPrayLightRecord::query()->with('toolsUserPrayer')
            ->fromSub($dayQuery->getQuery(), 'b')
            ->selectRaw('*, max(count_days) max_days')
            ->orderByDesc('count_days')
            ->orderBy('created_at')
            ->groupBy('user_id')
            ->limit($perPage)
            ->offset(($page - 1) * $perPage)
            ->get();
    }

    public static function encrypt(ToolsUserPrayer $prayer)
    {
        return Crypt::encrypt(serialize(['user_id' => $prayer->user_id, 'id' => $prayer->id]));
    }

    public static function decrypt(string $content)
    {
        return unserialize(Crypt::decrypt($content));
    }
}
