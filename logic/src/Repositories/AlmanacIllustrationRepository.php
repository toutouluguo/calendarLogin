<?php

declare(strict_types=1);

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\AlmanacIllustration;
use Carbon\Carbon;
use com\nlf\calendar\Lunar;

class AlmanacIllustrationRepository
{
    public function getIllustration()
    {
        return [
            'centenary'    => $this->centenary(),
            'illustration' => $this->getByYear(),
            'dimujing'     => $this->getDimujingListByYear(),
        ];
    }

    /**
     * 获取指定年插图.
     */
    public function getByYear(string $year = ''): \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel|null
    {
        if (empty($year)) {
            $year = now()->year;
        }

        return AlmanacIllustration::query()->where('year', $year)->first();
    }

    /**
     * 连续三年地母经.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getDimujingListByYear(string $year = '')
    {
        if (empty($year)) {
            $y = Carbon::now();
        } else {
            $y = Carbon::createFromDate($year);
        }
        $years = [$y->subYear()->year, $y->addYear()->year, $y->addYear()->year];

        return AlmanacIllustration::query()->whereIn('year', $years)->orderBy('year')->get(['year', 'gan_zhi', 'dimujing']);
    }

    /**
     * 百岁图.
     */
    public function centenary(): array
    {
        $lunar = Lunar::fromDate(now());
        $data  = [];
        for ($i = 1; $i <= 100; $i++) {
            $data[] = [
                'age'    => $i . '岁',
                'year'   => $lunar->getYear(),
                'ganZhi' => $lunar->getYearInGanZhi() . '年',
                'zodiac' => $lunar->getYearShengXiao(),
            ];
            $lunar = Lunar::fromDate(now()->addYears($i));
        }

        return $data;
    }
}
