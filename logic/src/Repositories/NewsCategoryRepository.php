<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\NewsCategory;

class NewsCategoryRepository
{
    public function getAll(): array|\Illuminate\Database\Eloquent\Collection
    {
        return NewsCategory::query()->orderByDesc('sort')->where('status', 1)->get();
    }

    public function getIdByLabel($label)
    {
        return NewsCategory::query()->where('label', $label)->value('id');
    }
}
