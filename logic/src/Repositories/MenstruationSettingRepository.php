<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\MenstruationSetting;
use Illuminate\Http\Request;

class MenstruationSettingRepository
{
    /**
     * 保存用户设置.
     *
     * @return \App\Models\BaseModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function getFromUserId(int $id)
    {
        return MenstruationSetting::query()->firstOrCreate([
            'user_id' => $id,
        ], [
            'days'            => 5,
            'cycle'           => 30,
            'is_home_display' => 0,
            'is_remind'       => 0,
            'remind_day'      => 5,
        ]);
    }

    /**
     * 保存用户设置.
     *
     * @return \App\Models\BaseModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function save(Request $request)
    {
        return MenstruationSetting::query()->updateOrCreate(
            ['user_id' => $request->user()->id],
            [
                'days'            => $request->days,
                'cycle'           => $request->cycle,
                'is_home_display' => $request->isHomeDisplay,
                'is_remind'       => $request->isRemind,
                'remind_day'      => $request->remindDay,
            ]
        );
    }
}
