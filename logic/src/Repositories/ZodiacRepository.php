<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\Zodiac;

class ZodiacRepository
{
    /**
     * 生肖运程详情.
     *
     * @return Zodiac|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function findByType(string $name)
    {
        return Zodiac::query()->where('type', $name)
            ->select(['type', 'month_fortune', 'summary', 'career', 'love', 'healthy', 'wealth'])
            ->first();
    }
}
