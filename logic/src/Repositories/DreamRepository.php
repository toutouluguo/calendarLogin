<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Enums\ApiResponseConst;
use CalendarLogic\Models\Dream;

class DreamRepository
{
    /**
     * 解梦搜索.
     *
     * @return mixed
     */
    public function search(string $keywords)
    {
        return Dream::query()->dream()
            ->where('name', 'like', "%{$keywords}%")
            ->take(10)
            ->get(['id', 'name']);
    }

    /**
     * 解梦分类.
     *
     * @return Dream[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function category()
    {
        return Dream::query()->where('parent_id', 0)->get(['id', 'name']);
    }

    /**
     * 分类详细.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function categoryDetail(int $id, int $pageNum = ApiResponseConst::PER_PAGE)
    {
        return Dream::query()->dream()->where('parent_id', $id)
            ->select(['id', 'name'])
            ->paginate($pageNum);
    }

    /**
     * 热门解梦.
     *
     * @return Dream[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function hotDream()
    {
        return Dream::query()->dream()
            ->inRandomOrder()
            ->take(9)
            ->get(['id', 'name']);
    }

    /**
     * 解梦详情.
     *
     * @return mixed
     */
    public function interpretation(int $id)
    {
        return Dream::query()->dream()->where('id', $id)->get(['id', 'name', 'content']);
    }
}
