<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\UserAuth;

class UserAuthRepository
{
    /**
     * 通过标识获取用户信息.
     *
     * @return \App\Models\BaseModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getByIdentifier(string $identifier, string $type)
    {
        return UserAuth::query()->with('user')->where('identifier', $identifier)->where('identity_type', $type)->first();
    }

    /**
     * Socialite拓展查询.
     *
     * @param $authInfo
     *
     * @return UserAuth|\App\Models\BaseModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function firstOrCreateFromSocialite($authInfo, $type)
    {
        $data['identity_type'] = $type;
        $data['identifier']    = $authInfo->id;
        $update['access_data'] = $authInfo->accessTokenResponseBody ?? [];
        $update['user_data']   = $authInfo->user                    ?? [];

        return UserAuth::query()->updateOrCreate($data, $update);
    }

    public function getByUserId(int $userId, string $type): UserAuth|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel|null
    {
        return UserAuth::query()->where('user_id', $userId)->where('identity_type', $type)->first();
    }

    /**
     * 解除第三方账号关联.
     *
     * @return bool|mixed|null
     */
    public function unbind(int $userId, string $type): mixed
    {
        return UserAuth::query()->where('user_id', $userId)->where('identity_type', $type)->delete();
    }
}
