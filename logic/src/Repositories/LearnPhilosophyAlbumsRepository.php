<?php

declare(strict_types=1);

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\LearnPhilosophy;
use CalendarLogic\Models\LearnPhilosophyAlbum;
use CalendarLogic\Models\LearnPhilosophyCategory;

class LearnPhilosophyAlbumsRepository
{
    /**
     * 展示页.
     */
    public function getSectionShow(int $id): \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
    {
        $learnPhilosophyCategory = LearnPhilosophyCategory::with(['LearnPhilosophyAlbums' => function ($query) {
            return $query->select(['id', 'learn_philosophy_category_id', 'cover', 'name', 'introduce'])->orderByDesc('sort')->orderbyDesc('id')->limit(3);
        }])->where('id', $id)->select(['name', 'bottom_tools', 'id'])->first();

        foreach ($learnPhilosophyCategory->LearnPhilosophyAlbums as &$learnPhilosophyAlbum) {
            $learnPhilosophyAlbum->learnPhilosophies = LearnPhilosophy::query()->inRandomOrder()->where('learn_philosophy_album_id', $learnPhilosophyAlbum->id)->limit(3)->get();
        }

        return $learnPhilosophyCategory;
    }

    /**
     * 专辑详情.
     *
     * @return \App\Models\BaseModel|\App\Models\BaseModel[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function getAlbumById(int $id)
    {
        return LearnPhilosophyAlbum::with('learnPhilosophyCategory')->find($id);
    }
}
