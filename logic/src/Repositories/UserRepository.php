<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UserRepository
{
    /**
     * 通过手机号获取用户.
     *
     * @return User|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getByPhone(string $phone)
    {
        return User::query()->where('phone', $phone)->first();
    }

    /**
     * 注册.
     *
     * @return User|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function register(array $data)
    {
        if (!isset($data['name']) && isset($data['phone'])) {
            $data['name'] = substr_replace($data['phone'], '****', 3, 4);
        }

        return User::query()->create($data);
    }

    /**
     * 用户基本信息修改.
     *
     * @return bool
     */
    public function updateBase(Request $request)
    {
        $data = [];
        foreach ($request->only(['name', 'avatar', 'gender', 'birthday', 'birthdayHour', 'personalizedSignature','shareName']) as $key => $item) {
            $data[Str::snake($key)] = $item;
        }
        $request->user()->update($data);

        return $request->user();
    }

    /**
     * 用户注销
     *
     * @return bool|null
     */
    public function cancel(Request $request)
    {
        return $request->user()->delete();
    }
}
