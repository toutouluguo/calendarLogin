<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\ConstellationPairing;
use CalendarLogic\Models\Horoscope;

class ConstellationRepository
{
    public function horoscope(string $constellation, string $tab): ?Horoscope
    {
        return Horoscope::query()->where([
            'constellation' => $constellation,
            'tab'           => $tab,
        ])->first();
    }

    public function pairing(string $male, string $female): ConstellationPairing
    {
        return ConstellationPairing::query()
            ->where('male', $male)
            ->where('female', $female)->first();
    }
}
