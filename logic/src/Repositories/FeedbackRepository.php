<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Enums\CacheNameConst;
use CalendarLogic\Models\Feedback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class FeedbackRepository
{
    // 分类  打开次数然后弹窗
    protected static array $positionClass = [
        'global' => [ // 全局
            15 => 4, // 打开2次之后弹窗
            16 => 10,
        ],

        'tool' => [ // 工具
            2  => 1,
            3  => 1,
            4  => 1,
            5  => 1,
            6  => 1,
            7  => 1,
            8  => 1,
            9  => 1,
            10 => 1,
            11 => 1,
            12 => 1,
            13 => 1,
            14 => 1,
        ],
    ];

    protected static int|float $ttl = 60 * 60 * 24 * 30;

//    protected static int|float $ttl = 60;

    public function __construct()
    {
        if ('test' == config('app.env')) {
            self::$ttl = 60;
        }
    }

    /**
     * 保存反馈.
     *
     * @return \App\Models\BaseModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function save(Request $request)
    {
        $this->setPopupCache($request->get('position', 1));

        $data = [
            'content'       => (string)$request->get('content', ''),
            'images'        => (array)$request->get('images', ''),
            'contact'       => (string)$request->get('contact', ''),
            'position'      => (int)$request->get('position', ''),
            'app_device_id' => device()->getId(),
            'ip'            => $request->ip(),
        ];

        return Feedback::query()->create($data);
    }

    /**
     * 反馈弹窗标记.
     *
     * @param Request $request
     *
     * @return void
     */
    public function mark(int $position)
    {
        $this->setPopupCache($position);
    }

    /**
     * 设置弹窗缓存.
     */
    protected function setPopupCache(int $position): void
    {
        if ($key = $this->getPositionClass($position)) {
            Cache::tags('cache')->remember($this->getPopupCacheName($key), self::$ttl, function () {
                return 1;
            });
        }
    }

    /**
     * 获取弹窗缓存.
     *
     * @return mixed|null
     */
    protected function getPopupCache(int $position)
    {
        if (!$key = $this->getPositionClass($position)) {
            return null;
        }

        return Cache::tags('cache')->get($this->getPopupCacheName($key));
    }

    /**
     * 获取位置分类.
     */
    protected function getPositionClass(int $position): ?string
    {
        foreach (self::$positionClass as $key => $class) {
            if (array_key_exists($position, $class)) {
                return $key;
            }
        }

        return null;
    }

    /**
     * 弹窗缓存名.
     */
    public function getPopupCacheName(string|int $key): string
    {
        return CacheNameConst::FEEDBACK . ':' . request()->header('imei') . ':' . $key;
    }

    /**
     * 是否弹窗.
     *
     * @return bool|void
     */
    public function popup(string $position)
    {
        $AppRepository = new AppRepository();
        // 全局开关
        $feedback_control = $AppRepository->getAppFeedbackControl();
        if (1 != $feedback_control) {
            return false;
        }

        $feedbackByChannel = $AppRepository->getAppFeedbackControlBYChannelAndVersion();
        if (!$feedbackByChannel) {
            return false;
        }
        if ($this->getPopupCache($position)) {
            return false;
        }

        $increment = false;
        if (15 == $position) {
            $increment = true;
        }
        $times = $this->positionAccumulation($position, $increment);
        if ($times < self::$positionClass[$this->getPositionClass($position)][$position]) {
            return false;
        }

        return true;
    }

    /**
     * 点击累计计算.
     */
    public function positionAccumulation(string $position, bool $increment = true): int
    {
        $cacheName = $this->getPopupCacheName($position);
        if (!Cache::tags('cache')->has($cacheName)) {
            Cache::tags('cache')->put($cacheName, 1, self::$ttl);
        } else {
            if ($increment) {
                Cache::tags('cache')->increment($cacheName);
            }
        }

        return Cache::tags('cache')->get($cacheName);
    }
}
