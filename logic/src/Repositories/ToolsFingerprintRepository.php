<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\ToolsFingerprint;

class ToolsFingerprintRepository
{
    protected $column = ['analyze',
        'fingerprint',
        'poetry',
        'character',
        'marriage',
        'profession',
        'healthy',
        'fortune', ];

    /**
     * 根据指纹搜索.
     *
     * @param $fingerprint
     *
     * @return \App\Models\BaseModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function getByFingerprint($fingerprint)
    {
        return ToolsFingerprint::query()
            ->select($this->column)
            ->where($fingerprint)->first();
    }

    /**
     * @return string[]
     */
    public function getColumn(): array
    {
        return $this->column;
    }

    /**
     * @param string[] $column
     */
    public function setColumn(array $column): self
    {
        $this->column = $column;

        return $this;
    }
}
