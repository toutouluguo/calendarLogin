<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\Saying;

class SayingRepository
{
    /**
     * 随机获取一条
     *
     * @return \App\Models\BaseModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getRandomOne()
    {
        return Saying::query()->inRandomOrder()->first();
    }
}
