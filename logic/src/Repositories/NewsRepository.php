<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Repositories;

use CalendarLogic\Models\News;
use CalendarLogic\Models\TempNews;

class NewsRepository
{
    protected static array $baseColumn = [
        'id',
        'title',
        'from',
        'release_at',
        'category_id',
        'author_name',
        'pics',
        'introduction',
    ];

    protected ?int $categoryId;
    protected bool $random = true;

    public function isRandom(): bool
    {
        return $this->random;
    }

    public function setRandom(bool $random): self
    {
        $this->random = $random;

        return $this;
    }

    /**
     * @return array|string[]
     */
    public static function getBaseColumn(): array
    {
        return self::$baseColumn;
    }

    /**
     * @param array|string[] $baseColumn
     */
    public static function setBaseColumn(array $baseColumn): void
    {
        self::$baseColumn = $baseColumn;
    }

    public function getCategoryId(): ?int
    {
        return $this->categoryId;
    }

    public function setCategoryId(?int $categoryId): self
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * 新闻列表.
     *
     * @param $categoryId
     * @param array
     *
     * @return
     */
    public function getList(int $page = 1, int $perPage = 10, $column = []): array|\Illuminate\Database\Eloquent\Collection
    {
        $column = !empty($column) ? array_merge($column, self::$baseColumn) : self::$baseColumn;

        $categoryId = $this->getCategoryId();
        $news       = News::with(['category' => function ($query) {
            $query->select('id', 'label');
        }])
            ->where(function ($query) use ($categoryId) {
                if ($categoryId) {
                    return $query->where('category_id', $categoryId);
                }
            })
            ->where('status', 1)
            ->limit($perPage)
            ->select($column);

        if ($this->isRandom()) {
            if (1 == $page) {
                $randomStartId = News::with(['category' => function ($query) {
                    $query->select('id', 'label');
                }])
                    ->where(function ($query) use ($categoryId) {
                        if ($categoryId) {
                            return $query->where('category_id', $categoryId);
                        }
                    })
                    ->orderByDesc('id')
                    ->where('status', 1)->offset(100)->limit(1)->value('id');
                $randomStartId = $randomStartId ?? 0;

                return $news->where('id', '>', $randomStartId)->inRandomOrder()->get();
            }
            $page = $page - 1;
        }

        return $news->offset(($page - 1) * $perPage)->orderByDesc('id')->get();
    }

    /**
     * @param $id
     * 新闻详情
     *
     * @return
     */
    public function getById($id, $column = ['content']): \Illuminate\Database\Eloquent\Model|array|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel|null
    {
        $column = !empty($column) ? array_merge($column, self::$baseColumn) : self::$baseColumn;

        $categoryId = $this->getCategoryId();

        return News::with('category')->where('id', $id)->where('status', 1)->where(function ($query) use ($categoryId) {
            if ($categoryId) {
                return $query->where('category_id', $categoryId);
            }
        })->first($column);
    }

    /**
     * 今日热点.
     *
     * @return
     */
    public function getTodayHot()
    {
        return News::query()->where('category_id', 12)
            ->where('status', 1)
            ->where('show_start_at', '<=', now())
            ->where('show_end_at', '>=', now())
            ->select(['id', 'title', 'introduction'])
            ->orderByDesc('id')
            ->first();
    }

    public function getTempNews(int $limit = 50)
    {
        return TempNews::query()->limit($limit)->orderByDesc('sort')->get();
    }
}
