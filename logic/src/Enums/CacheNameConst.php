<?php

declare(strict_types=1);

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Enums;

class CacheNameConst
{
    // config
    public const APP_SETTING = 'config:appSetting';

    // cache
    public const FEEDBACK        = 'cache:feedback';
    public const DAILY_SIGNATURE = 'cache:dailySignature';
}
