<?php

/*
 * This file is part of the calendar/api
 * (c) dev-php
 */

namespace CalendarLogic\Enums;

class ApiResponseConst
{
    public const PER_PAGE = 20;

    public const CODE_SUCCESS = 200;

    public const CODE_ERROR = 501;
}
